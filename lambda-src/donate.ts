import { stripe } from './lib/clients/stripe'
import Stripe from 'stripe'

interface DonateRrequestData {
  amount: string
  customAmount: string
  duration: 'once' | 'monthly'
  email: string
  giveInHonorOf: boolean
  giveInHonorOrMemoryOf: string
  inHonorOrMemoryOf: string
  giveToEndowmentFund: boolean
  note: string
  name: string
  paymentMethod: Stripe.PaymentMethod
}

interface Metadata extends Stripe.MetadataParam {
  name: string
  email: string
  inHonorOf: string | null
  inMemoryOf: string | null
  giveToEndowmentFund: string | null
  note: string | null
}

const DONATION_PRODUCT_NAME = 'VFC Donation'
const DONATION_PRODUCT_ID = 'vfc-donation'
const DONATION_PLAN_NICKNAME_PREFIX = 'VFC Monthly Donation'
const DONATION_PLAN_ID_PREFIX = 'vfc-monthly-donation'

const DEFAULT_METADATA: Metadata = {
  name: '',
  email: '',
  inHonorOf: null,
  inMemoryOf: null,
  giveToEndowmentFund: null,
  note: null,
}

const createDonation = async (data: DonateRrequestData) => {
  const {
    name,
    email,
    amount,
    customAmount,
    giveInHonorOf,
    giveInHonorOrMemoryOf,
    inHonorOrMemoryOf,
    paymentMethod,
    giveToEndowmentFund,
    note,
  } = data
  const donationAmount = Number(amount || customAmount)
  const metadata: Metadata = {
    ...DEFAULT_METADATA,
    email,
    name,
    giveToEndowmentFund: giveToEndowmentFund ? 'true' : null,
    inHonorOf:
      giveInHonorOf && giveInHonorOrMemoryOf === 'inHonorOf'
        ? inHonorOrMemoryOf
        : null,
    inMemoryOf:
      giveInHonorOf && giveInHonorOrMemoryOf === 'inMemoryOf'
        ? inHonorOrMemoryOf
        : null,
    note,
  }
  return stripe.paymentIntents.create({
    amount: donationAmount * 100,
    confirm: true,
    currency: 'usd',
    description: DONATION_PRODUCT_NAME,
    metadata,
    payment_method: paymentMethod.id,
    receipt_email: email,
  })
}

const createDonationSubscription = async (data: DonateRrequestData) => {
  const {
    name,
    email,
    amount,
    customAmount,
    giveInHonorOf,
    giveInHonorOrMemoryOf,
    inHonorOrMemoryOf,
    paymentMethod,
  } = data
  const donationAmount = Number(amount || customAmount)
  const metadata: Metadata = {
    email,
    name,
  }
  if (giveInHonorOf && giveInHonorOrMemoryOf === 'inHonorOf') {
    metadata.inHonorOf = inHonorOrMemoryOf
  } else if (giveInHonorOf && giveInHonorOrMemoryOf === 'inMemoryOf') {
    metadata.inMemoryOf = inHonorOrMemoryOf
  }
  let product: Stripe.Product
  try {
    product = await stripe.products.retrieve(DONATION_PRODUCT_ID)
  } catch (error) {
    product = await stripe.products.create({
      id: DONATION_PRODUCT_ID,
      name: DONATION_PRODUCT_NAME,
      type: 'service',
    })
  }
  const planId = `${DONATION_PLAN_ID_PREFIX}-amount-${donationAmount}`
  let plan
  try {
    plan = await stripe.plans.retrieve(planId)
  } catch (error) {
    const { statusCode } = error as Stripe.StripeError
    if (statusCode !== 404) {
      throw error
    }
    const planData: Stripe.PlanCreateParams = {
      id: planId,
      nickname: `${DONATION_PLAN_NICKNAME_PREFIX} - $${donationAmount}`,
      product: product.id,
      amount: donationAmount * 100,
      currency: 'usd',
      interval: 'month',
      usage_type: 'licensed',
    }
    plan = await stripe.plans.create(planData)
  }
  const customer = await stripe.customers.create({
    email,
    invoice_settings: {
      default_payment_method: paymentMethod.id,
    },
    metadata,
    name,
    payment_method: paymentMethod.id,
  })
  const subscription = await stripe.subscriptions.create({
    customer: customer.id,
    items: [{ plan: plan.id }],
    metadata,
  })
  return subscription
}

export const handler = async event => {
  if (event.httpMethod !== 'POST') {
    return {
      statusCode: 405,
      body: JSON.stringify({ message: 'Method Not Allowed' }, null, 2),
    }
  }
  try {
    const data = JSON.parse(event.body) as DonateRrequestData
    const { duration } = data
    let response
    switch (duration) {
      case 'monthly':
        response = await createDonationSubscription(data)
        break
      default:
        response = await createDonation(data)
    }
    return {
      statusCode: 200,
      body: JSON.stringify({ message: 'Success' }, null, 2),
    }
  } catch (error) {
    const { message, statusCode } = error as Stripe.StripeError
    return {
      statusCode: statusCode || 500,
      body: JSON.stringify({ message }, null, 2),
      error: error,
    }
  }
}
