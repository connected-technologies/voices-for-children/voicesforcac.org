require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
})

const config = require('./site-config.json')
const siteUrl = process.env.SITE_URL || 'http://localhost:8000'
const trackingIds = [process.env.GA_TRACKING_ID || null]
const proxy = require('http-proxy-middleware')

module.exports = {
  pathPrefix: process.env.PATH_PREFIX || '/',
  siteMetadata: {
    description: config.description,
    footer: config.footer,
    logo: config.logo,
    social: config.social,
    siteUrl,
    title: config.title,
  },
  plugins: [
    `gatsby-plugin-typescript`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/static/media`,
        name: 'images',
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/data`,
        name: 'data',
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages',
        // ignore: process.env.NODE_ENV === `production` && [
        //   `**/___developer/**/*`,
        // ],
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        commonmark: false,
        footnotes: true,
        pedantic: true,
        gfm: true,
        plugins: [
          'gatsby-remark-relative-images',
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1600,
            },
          },
          'gatsby-remark-embed-video',
          'gatsby-remark-responsive-iframe',
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        // You can add multiple tracking ids and a pageview event will be fired for all of them.
        trackingIds,
        gtagConfig: {
          anonymize_ip: true,
          cookie_expires: 0,
        },
        pluginConfig: {
          head: false,
          respectDNT: true,
          exclude: ['/___developer/**'],
        },
      },
    },
    {
      resolve: `gatsby-plugin-create-client-paths`,
      options: { prefixes: [`/make-a-referral/*`] },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Voices for Children`,
        short_name: `Voices for Children`,
        start_url: `/`,
        background_color: `#037cbc`,
        theme_color: `#037cbc`,
        display: `minimal-ui`,
        icon: `static/media/favicon.png`,
      },
    },
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-offline`,
    `gatsby-plugin-react-helmet`,
    'gatsby-transformer-json',
    'gatsby-plugin-sass',
    'gatsby-plugin-catch-links',
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        exclude: ['/___developer/*'],
      },
    },
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: siteUrl,
        sitemap: `${siteUrl}/sitemap.xml`,
        env: {
          production: {
            policy: [{ userAgent: '*', disallow: ['/___developer/'] }],
          },
          development: {
            policy: [{ userAgent: '*', disallow: ['/'] }],
            sitemap: null,
            host: null,
          },
        },
      },
    },
  ],
  developMiddleware: app => {
    app.use(
      '/.netlify/functions/',
      proxy({
        target: 'http://localhost:9000',
        pathRewrite: {
          '/.netlify/functions/': '',
        },
      })
    )
  },
}
