---
template: event
image: "/media/superhero-run-logo-no-year.png"
title: Superhero 5K Race & 1K Fun Run
start_date: 2023-09-16T09:00:00-04:00
end_date:
location: ''
address: ''
address_link: ''
blocks:
- template: block__feature
  component: feature
  background_image_opacity: 1
  orientation: normal
  featured_content: ''
  image: "/media/VFC_Superhero_5K_Run_LOGO_2023.png"
  title: Become a Super-Sponsor!
  content: |-
    Take part in one of the most family-friendly events in Flint and get your business or agency exposure. Please contact (810) 238-3333 ext. 227 or andrea@voicesforcac.org if you are interested in becoming a sponsor of the 2023 event.

    <a href="https://drive.google.com/file/d/1d_ivp3gFmrCfop61ba1TZsoJ-MxoACy5/view?usp=share_link" class="button is-primary" target="_blank">Sponsorship Guide</a>
  background: Light Blue
  background_image: ''
- template: block__content
  component: content
  content: |-
    ## So what is a virtual race?

    It is a flexible, non-competitive, 5K or 1K that you can do wherever you are and however you want. You can bike, swim, run, walk, or fly. There's no specific location, route, or time. Just do your superhero best between on or leading up to September 16th then post about your run on social media to share the event as far as possible.

    ## Registration
    <a href="https://runsignup.com/Race/MI/Flint/FlintSuperhero" class="button is-primary" target="_blank">Register Here</a>

    Registration on the day of the event will be an additional $10. Register before July 1st, and save $5!

    * **Virtual/ In-Person 5K Run & 5K Walk**
    $35
    * **Virtual/ In-Person 1K Fun Run**
    $30

    ### What do I get for my registration fee?

    You get to play a critical role in helping local child survivors of human trafficking, sexual abuse, physical abuse, and neglect, get the services they need to heal and thrive!

    All participants will receive a finisher medal!

    This year, all 5K event participants will also receive an exclusive race T-shirt!

    Additional shirts are available for purchase as an add-on to your registration but are only available until September 1, so don’t miss out! If you’ve already registered and would like to purchase shirts before the cut-off, reach out to ethan@voicesforcac.org. Check out other merch available at our online store for more ways to support Voices for Children.


    ### Packet Pick Up
    Beat those lines the morning of the event by getting your race packet early. Included will be your race number/ run-bib to wear on your in-person or virtual run, and t-shirts that you’ve selected and/or ordered. If you selected virtual you will also be given your participation medal. Be sure to share photos and videos to spread the word!

    **Friday, September 15**  
    11:00 AM - 1:30 PM & 3:00 - 5:30 PM

    Complete Runner   
    3170 South Linden Rd.   
    Flint, MI 48507


    ## What to know about the event:
    ### Race Time
    The 5K Runners will start first, kicking off at 9:00 AM. 5K Walkers will follow that event. The 1K Event will begin at 10:30 AM or after the last walker finishes their event.

    If you are registering the morning of the race in person, or picking up your race packet the day of, be sure to come early. The packet pick-up/ registration table will be open at 8:00 AM.

    ### Racecourse
    Our racecourse will be the same as previous events if you've joined us before. Check our racecourse map [here](https://www.google.com/maps/d/u/0/viewer?ll=43.02406109359319%2C-83.67505252540258&z=18&mid=14suZg-zJvW5UwSH5eIbR64q9JdPgKYI). We will be kicking off on East 1st Street, between the Flint Farmers' Market and Wilson Park on UofM's campus. The course will take us through Mott Community College, Applewood Estate, and the Flint Cultural Center.

    ### Parking
    Parking is available in a few different areas. The [Harrison Street Parking Ramp](https://www.google.com/maps/place/Harrison+Street+Parking+Ramp/@43.0175687,-83.690719,15z/data=!4m2!3m1!1s0x0:0x89d1a0ff44a95f5b?sa=X&ved=2ahUKEwiNkqS72O3yAhUqAp0JHU4bD8oQ_BIwG3oECFQQBQ), Lot A & Lot T, Harrison Street Parking Deck, Mill Street Parking Deck, and University Pavillion Parking Deck, are all available to participants for free. The Flint Flat Lot is available for a $5 flat fee, as well. Please note, the Flint Farmers' Market parking is reserved only for market patrons and cannot be used for event parking, thank you. [See a map of free parking available across UofM's Campus](https://drive.google.com/file/d/1mIZjyMjbTKdkTI9HpATOxrW8GOkdFSYt/view?usp=drive_link).

    ### Dress & Share
    We really encourage you to join in the fun and dress as your favorite superhero! It's a lot of fun to see everyone decked out and ready to save the day. We invite you and your group/family/coworkers, to dress up and share photos and videos of you participating in the virtual or in-person events. Share your photos by tagging @voicesforcac and @FlintSuperhero5k on Facebook and please use #SuperHeroFlint2023 #SuperHeroRunFlint2023 #VoicesforChildren #VCAC on all social media posts.

    We love seeing all the photos and videos of runners! See our Facebook event page as well to see more. Sharing your photos and videos is a great way to spread the message about helping our community's most vulnerable.

    ### Severe Weather Policy
    The safety of our athletes, spectators, and volunteers is our primary concern. In the event of inclement weather, every attempt will be made to preserve the safety of everyone involved and complete the race as scheduled.

    The event will not be canceled due to rain alone. Race officials reserve the right to delay or shorten the race based on heavy rain. In the event of severe weather (eg: lightning, tornado, high winds) the event may be delayed as race officials consult public safety officers. If there is more than a reasonable delay, or if conditions persist, race officials reserve the right to shorten or cancel the race. Participants should seek safe shelter should severe weather develop during the race.

    Please check our website, Facebook Page, and your email, before the race for any changes or cancellations. If you are already at the event, listen to the announcer about any weather changes that may occur. Entry fees will not be refunded in the event the race is canceled. Entry fees will not be credited to future events. Thank you for understanding as this is a fundraiser intended to support our services & programs.

    ## Sponsors

    ### T-Shirt Sponsor
    <div class="columns is-vcentered">
      <div class="column is-one-third has-text-centered">
        <img src="/media/elga.png" alt="Elga" />
      </div>
    </div>

    ### Finish Line Photo Sponsor
    <div class="columns is-vcentered">
      <div class="column is-one-third has-text-centered">
        <img src="/media/hap.png" alt="HAP" />
      </div>
    </div>

    ### Finish Line Banner Sponsor
    <div class="columns is-vcentered">
      <div class="column is-one-third has-text-centered">
        <img src="/media/police-chiefs.jpg" alt="Michigan Association of Chiefs of Police" />
      </div>
    </div>

    ### Race Bib Sponsors
    <div class="columns is-vcentered">
      <div class="column is-one-third has-text-centered">
        <img src="/media/pioneer-state-mutual.jpg" alt="Pioneer State Mutual" />
      </div>
    </div>

    ### Start Line Pole Sponsors
    <div class="columns is-vcentered">
      <div class="column is-one-third has-text-centered">
        <img src="/media/ctg-insurnace.png" alt="CTG Insurance" />
      </div>
      <div class="column is-one-third has-text-centered">
        <img src="/media/ameriprise.jpg" alt="Ameriprise Financial Services LLC" />
      </div>
    </div>

    ### Finish Line Pole Sponsors
    <div class="columns is-vcentered">
      <div class="column is-one-third has-text-centered">
        <img src="/media/gisd.jpg" alt="Genesee Intermediate School District" />
      </div>
      <div class="column is-one-third has-text-centered ">
        <img src="/media/total-benefit-systems.png" alt="Total Benefit Systems" />
      </div>
    </div>

    ### Mile Marker Sponsors
    <div class="columns is-vcentered">
      <div class="column is-one-quarter has-text-centered">
        <img src="/media/mukkamala.jpg" alt="Drs. Bobby & Nita Mukkamala" />
      </div>
      <div class="column is-one-quarter has-text-centered">
        <img src="/media/molina.jpg" alt="Molina Health Care" />
      </div>
    </div>

    ## Thank you!
    Thank you so much for your support and for joining us for the 11th Annual Superhero Fun Run!
- template: block__gallery
  component: gallery
  images:
  - title: Superhero8
    image: "/media/59022895_2074035562719315_4482258471747584000_o.jpg"
  - title: Superhero7
    image: "/media/58787500_2072350142887857_2369895544421089280_o.jpg"
  - title: Superhero6
    image: "/media/58420263_2074035556052649_6417019974488948736_o.jpg"
  - title: Superhero5
    image: "/media/57251147_2074035546052650_5435122474175954944_o.jpg"
  - title: Superhero4
    image: "/media/50860326_1938555609600645_5665261502093852672_o.jpg"
  - title: Superhero3
    image: "/media/50790008_1938555639600642_2263191213710508032_o.jpg"
  - title: Superhero2
    image: "/media/50739411_1938555622933977_7278724898082521088_n.jpg"
  - title: Superhero1
    image: "/media/50639539_1938555636267309_8192811358782226432_o.jpg"
  title: ''
  description: ''

---
## 2023 Superhero 5K Race & 1K Fun Run

<a href="https://runsignup.com/Race/MI/Flint/FlintSuperhero" class="button is-primary" target="_blank">Register Here</a>

Your registration for the Superhero 5k or 1k Fun Run goes directly to help child survivors of human trafficking, abuse, and neglect by providing them with hope, help, and healing. Voices for Children Advocacy Center serves the Flint Area, and all of Genesee, and Shiawassee Counties. All funds raised will benefit children who are survivors of human trafficking, abuse, or neglect.

The Superhero 5K Race & 1K Fun Run will have both in-person and virtual registration options. Scroll below to find information on sponsorships, frequently asked questions, and more on the event.
