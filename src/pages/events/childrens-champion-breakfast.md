---
template: event
image: "/media/childrens-champion-awards-logo.png"
title: Children's Champion Awards
start_date: 2024-03-21T07:30:00.000-04:00
end_date: 2024-03-21T10:00:00.000-04:00
location: Riverfront Conference Center
address: 1 Riverfront Plaza, Flint, MI 48502
address_link: https://www.google.com/maps/place/Riverfront+Conference+Center/@43.0176945,-83.6955226,17z/data=!3m1!4b1!4m5!3m4!1s0x8823821689036081:0xff0446ea78a6a185!8m2!3d43.01794!4d-83.69278
blocks:
- template: block__feature
  component: feature
  background_image_opacity: 1
  orientation: normal
  image: "/media/ccba-2023-eventbrite-image2.jpg"
  background: Light Blue
  title: 2024 Children's Champion Awards
  content: |-
    Thank you again to everyone who attended, supported, or tuned-in to, the 2024 Children's Champion Awards! Congratulations again to all of the deserving nominees! Please join our mailing list below to stay up-to-date on when 2024 nominations and scholarships are available! **Save the date for 2024's Award Ceremony, Thursday, March 21, 2024.**

     Interested in sponsoring?

    <a href="/media/ccba-2024-sponsorship-guide.pdf" class="button is-primary" target="_blank">Sponsorship Guide</a>

    Nominations for Children's Champion Awards now open until January 2024. Nominate someone using the links below.
- template: block__content
  component: content
  content: |-
      ## Children's Champion 2024 Nominations 
    
      We are so excited to share that this year we are celebrating 29 years of honoring ordinary people for doing extraordinary things for children in Genesee County. Please click the links below to nominate your favorite people today. 
      
      ### Active and Healthy Leader Award
      This award is for any adult who promotes participatory health and fitness activities that benefit the children of Genesee County. e.g. Running a wellness program for children, serving as a coach for a school sport and/or supporting Special Olympics or other organized sporting events for youth.
    
      <a href="https://docs.google.com/forms/d/e/1FAIpQLSclmPAJDvYhaHTwxBdbYhwyH3I8br9MkHBxegkS_BiRjwCZhg/viewform" class="button is-primary" target="_blank">Nominate Someone</a>
      
      ### Exceptional Business Award 
      This award is presented to a for-profit business providing special opportunities or resources for children and youth. e.g. special programming, scholarships, medical or dental care for children, camps, and/or other opportunities that would otherwise not be available. 
      
      <a href="https://docs.google.com/forms/d/e/1FAIpQLSddAPzeAbyjhbZU47UBe8au_lupRIRAM_0ox972cp51NRcgUg/viewform" class="button is-primary" target="_blank">Nominate Someone</a>
      
      ### Outstanding Youth Award
      The Robert E. Weiss Outstanding Youth Award is for a high school student who has done something special to help make Genesee County a better community for children. e.g. Mentoring/tutoring younger children, organizing initiatives to benefit other high school students and/or volunteering for organizations that help children.
      
      <a href="https://docs.google.com/forms/d/e/1FAIpQLScpjEefy6JMdNWck6GgXNFoaPSl2GXKT6dXl2ZCrm-DCmDNaQ/viewform" class="button is-primary" target="_blank">Nominate Someone</a>
      
      ### Lifetime Achievement Award 
      The Lifetime Achievement Award is for a person who has dedicated at least 20 years towards the advancement of helping children. e.g. volunteering with youth, serving as a foster grandparent and/or hosting fundraising events to benefit youth.
      
      <a href="https://docs.google.com/forms/d/e/1FAIpQLSeK4AYc-3JZTwcA8-Yu_bW9p4-bP3gweShfTdZ6bZh4CdKGYg/viewform" class="button is-primary" target="_blank">Nominate Someone</a>
      
      ### Caring Adult Award
      The Roy E. Peterson Caring Adult Award is for any adult (volunteer, employee or advocate) who goes beyond the call of duty to provide services to children or youth in Genesee County. e.g. Those working tirelessly in a field that benefits children, leading a faith-based youth group and/or serving on boards of youth-focused organizations. 2 awards available
      
      <a href="https://docs.google.com/forms/d/e/1FAIpQLSeAMR-A6Gvu4xPjPDVp6KWGN8mKBCRmy5NwRL0ZdfPdkyhDmg/viewform" class="button is-primary" target="_blank">Nominate Someone</a>
      
      ### Service Organization of the Year Award
      This award is presented to an organization providing community service to youth with volunteerism opportunities or providing employees/members with opportunities to volunteer for organizations improving the lives of children. e.g. Any program or Institution that supports and integrates meaningful community service engaging or in service of youth. Houses of worship, youth -related groups, financial institutions, service organizations.
      
      <a href="https://docs.google.com/forms/d/e/1FAIpQLSeUh5k5xMZvcra0urer0_9Fbs_6xXUGiZDvtBhZTV4nwwjRvA/viewform" class="button is-primary" target="_blank">Nominate Someone</a>
- template: block__content
  component: content
  content: "# Congratulations Heroes!\n\n## 2023 Children's Champions\n\n##### Bob
    Emerson Scholarship,   \npresented by the Community Foundation of Greater Flint
    \ \nMadison Bright & Colleen Blackwood\n\n##### Crim Fitness Foundation Active
    & Healthy Leader Award,   \npresented by Crim Fitness Foundation  \nElizabeth
    Wise\n\n##### Exceptional Business Award,   \npresented by JP Morgan Chase  \nKids
    Like Mine\n\n##### Exceptional Business Award,   \npresented by Yeo & Yeo CPAs
    & Business Consultants  \nCrossover Outreach\n\n##### Karen Church Lifetime Achievement
    Award,   \npresented by McLaren Flint  \nHonorable Duncan Beagle\n\n##### Multidisciplinary
    Team Award,   \npresented by ELGA Credit Union  \nMonica Gildner\n\n##### Multidisciplinary
    Team Award,   \npresented by ELGA Credit Union  \nSgt. Michael Bernard\n\n#####
    Robert & Vickie Weiss Outstanding Youth Award,   \npresented by Genesee Health
    System  \nVictoria Griffith\n\n##### Roy E. Peterson Caring Adult Award,   \npresented
    by Hurley Medical Center  \nVanita Wilson\n\n##### Roy E. Peterson Caring Adult
    Award,   \npresented by the Community Foundation of Greater Flint  \nKyle Ferguson\n\n#####
    Volunteer Engagement to Serve Youth Award,   \npresented by McLaren Health Plan
    \ \nFoster Love"
- template: block__content
  component: content
  content: |-
    # Children's Champion Awards

    The Children’s Champion Awards were created in 1995 to recognize examples of those in the community doing good things for children. The awards are designed to distinguish and thank individuals, businesses, government agencies, and organizations for their tremendous contributions toward making children a priority in Genesee County.

    Be sure to subscribe to our mailing list below if you've not already done so, and follow us on Facebook to stay up-to-date.

    <a href="https://www.facebook.com/voicesforcac" class="button is-primary" target="_blank">See us on Facebook</a>

    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css"> <style type="text/css"> #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; } /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block. We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. _/ </style> <div id="mc_embed_signup"> <form action="https://voicesforcac.us9.list-manage.com/subscribe/post?u=30bec541a81700f9129735e1b&id=98d7ed5532" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate> <div id="mc_embed_signup_scroll"> <h2>Subscribe for Updates</h2> <div class="indicates-required"><span class="asterisk">_</span> indicates required</div> <div class="mc-field-group"> <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span> </label> <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL"> </div> <div class="mc-field-group"> <label for="mce-FNAME">First Name </label> <input type="text" value="" name="FNAME" class="" id="mce-FNAME"> </div> <div class="mc-field-group"> <label for="mce-LNAME">Last Name </label> <input type="text" value="" name="LNAME" class="" id="mce-LNAME"> </div> <div id="mce-responses" class="clear"> <div class="response" id="mce-error-response" style="display:none"></div> <div class="response" id="mce-success-response" style="display:none"></div> </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups--> <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_30bec541a81700f9129735e1b_98d7ed5532" tabindex="-1" value=""></div> <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div> </div> </form> </div> <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames\[0\]='EMAIL';ftypes\[0\]='email';fnames\[1\]='FNAME';ftypes\[1\]='text';fnames\[2\]='LNAME';ftypes\[2\]='text';fnames\[3\]='ADDRESS';ftypes\[3\]='address';fnames\[4\]='PHONE';ftypes\[4\]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script> <!--End mc_embed_signup-->
- template: block__content
  component: content
  content: "## Frequently Asked Questions:\n\n#### What do I do when I arrive?\n\nIf
    you are a sponsor See your table number in the message above. When you arrive
    there will be a board with the table numbers on it to help you get to your table.
    You can also fly over to our Help Desk for assistance.\n\nIf you are a nominee
    or ticket holder - you can sit in any of the open seating. Our Marvel and DC superhero
    volunteers will be there to help you. Be sure to grab your nomination certificate
    if you were nominated for an award!\n\n#### Do I need to print a ticket?\n\nNo,
    there are no physical tickets. If you RSVP'd or purchased a seat you are all set,
    you'll just need to give your name or company at the check-in table. Thank you
    for your support!\n\n#### Parking\n\nThe Downtown Development Authority is our
    2023 Parking Sponsor. Parking is available both at the attached University of
    Michigan Riverfront Conference Center Parking Ramp, and the Rutherford Parking
    Lot off of Beach St.\n\nThe University of Michigan Parking Ramp entrance is located
    in the front of the building, off Saginaw Street and W. Union Street. After you
    enter the parking structure, you can take the elevator or stairs to the ground
    floor. The entry point to the elevator is located on the third and fourth levels
    of the structure. You will not be able to enter the building through the housing
    skywalk which is on the third floor, as this is reserved for residence hall students
    with swipe card access. After you make your way to the ground floor, or if you
    parked at the Rutherford Lot, you will walk across W. Union Street and enter the
    main entrance to Riverfront Conference Center. The conference center is to the
    left of the main entrance lobby.\n\n#### What about breakfast?\n\nBreakfast, and
    cold and hot beverages, will be provided. Refreshments will be available at 7:30
    am, the program will begin promptly at 8:00 am. \n\n#### If I was nominated, how
    will I know if I won?\n\nWinners will be announced live during the program and
    will be asked to go to the stage. If you win, you will have an _optional_ opportunity
    to speak for one minute. Please listen closely when the category you were nominated
    for is announced to find out if you are a winner."
- template: block__columns
  component: columns
  title: 2024 Event Partner
  description: ''
  columns:
  - template: block__column
    component: column
    alignment: ''
    content: <img alt="United Way Logo" src="/media/United-Way.jpg" />
    width: One Half
- template: block__map
  component: map
  latitude: '43.0176944'
  longitude: "-83.6978186"
  popup_content: Riverfront Banquet Center - Downtown Flint - 1 Riverfront Plaza,
    Flint, MI 48502

---
