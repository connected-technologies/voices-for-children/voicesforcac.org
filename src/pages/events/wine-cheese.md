---
template: event
image: "/media/wine-and-cheese-marketing-photo2.jpg"
title: Wine & Cheese
start_date: 2024-04-25T18:00:00-04:00
end_date: 
location: Wrought Iron Grill
address: '317 S Elm St #201, Owosso, MI 48867'
address_link: https://goo.gl/maps/JHw6LD9KPyfoRjz47
blocks:
- template: block__feature
  component: feature
  background_image_opacity: 1
  orientation: normal
  title: 15th Annual Wine & Cheese
  content: |-
    Join Voices for Children, **Thursday, April 25, 2024**, at the Wrought Iron Grill for our 15th Annual Wine & Cheese event to benefit abused and neglected children in Shiawassee County. This fun night out includes silent and live auctions, wine and cheese pairings, and more. Get your tickets today! Be sure to invite your friends and family on [Facebook](https://fb.me/e/3ixSDjjee) too!

    <a href="https://www.eventbrite.com/e/wine-cheese-2023-tickets-558080302207" class="button is-primary" target="_blank">Purchase Tickets Today</a>

    There's still time to sponsor this event! Get your business/ brand recognition at this event and show your commitment to keeping Shiawassee County children safe from abuse and neglect. Check out this event, and others, and learn more.

    <a href="https://bit.ly/sponsorVCAC23" class="button is-primary" target="_blank">See our Sponsorship Guide</a>
  image: "/media/wine-and-cheese-2023-web-banner.jpg"
  background: Light
  featured_content: ''
  background_image: ''
- template: block__feature
  component: feature
  background_image_opacity: 1
  orientation: reverse
  content: "# [**Wrought Iron Grill**](https://wroughtirongrill.com/)"
  image: "/media/032218-wrought-iron-grill-10-year-anniversary-slide[1].jpg"
  background: Dark
  title: ''
  featured_content: ''
  background_image: ''
- template: block__columns
  component: columns
  title: Thank you to our Event Sponsors!
  columns:
  - template: block__column
    component: column
    content: |-
      ## Child Hero Sponsor

      ![](/media/georgia-pacific-stack-logo-1.jpg)
    width: One Half
    alignment: Left
  description: ''
- template: block__columns
  component: columns
  title: ''
  columns:
  - template: block__column
    component: column
    width: One Third
    alignment: Left
    content: |-
      ### Child Advocate Sponsors

      ![](/media/servpro_logo-1.jpg)![](/media/stifel-logo_540-01-1.png)

      ![](/media/logo-lrg-1.jpg)

      ![](/media/08026-logo-lg-md-publish-1.png)

      ![](/media/logo-1.png)

      ![](/media/gpilogo-new-1.png)
  description: ''
- template: block__map
  component: map
  latitude: '42.994949'
  longitude: "-84.17782"
  popup_content: "[Wrought Iron Grill](https://goo.gl/maps/QXn4Q3RTT3zbwtfy6)"

---
