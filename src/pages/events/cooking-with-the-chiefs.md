---
template: event
image: "/media/cooking-with-the-chiefs-marketing-photo2.jpg"
title: Cooking with the Chiefs
start_date: TBD
end_date: 
location: Genesys Banquet & Conference Center
address: 805 Health Park Boulevard, Grand Blanc Township, MI, 48439
address_link: https://goo.gl/maps/DfZa3Kw8rcHfifi37
blocks:
- template: block__feature
  component: feature
  background_image_opacity: 1
  orientation: normal
  title: A Delicious New Event!
  content: |-
    Genesee County Chiefs of Police, and Fire Departments, partnered with local restaurants and caterers to create a tasting menu with their favorite dishes. H.A.P and local Police & Fire Chiefs supported Voices for Children Advocacy Center at Genesys Banquet & Conference Center. 
  image: "/media/cooking-with-the-chiefs-marketing-photo1.jpg"
  background: Light
  featured_content: ''
  background_image: ''
  
---
