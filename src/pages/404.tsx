import React from 'react'

import Layout from '../components/layout'
import Link from '../components/link'
import PageTitle from '../components/page-title'
import SEO from '../components/seo'

const PageNotFound = () => {
  return (
    <Layout>
      <SEO title="Page Not Found" />
      <PageTitle title="Page Not Found" />
      <section className="section page-content">
        <main className="container content-container">
          <div className="content">
            We&apos;re sorry, but we could not find the page you are looking
            for.&nbsp;
            <Link to="/">Head back home</Link>.
          </div>
        </main>
      </section>
    </Layout>
  )
}

export default PageNotFound
