---
template: page_with_blocks
title_background: Purple
title_background_image_opacity: 0.25
position: Above
title: Training
subtitle: ''
title_background_image: "/media/kristin-brown-3kBtYrZ0-hw-unsplash.jpg"
blocks: []

---
# Schedule a Training Today!

Voices for Children provides professional enrichment trainings on various child welfare topics. These trainings are provided at no charge, and are available for groups of 10 or more. Right now, all trainings are being presented virtually. If you have a group that would be interested in having Voices for Children present on any of the following topics please fill out the form below.

<a href="https://docs.google.com/forms/d/e/1FAIpQLScYTdi4RpUpgHQaKhl4ngPydjIQLPmTXZAGAc5tgaf-3mv4RQ/viewform" class="button is-primary" target="_blank">Schedule a Training Today</a>

# Available Trainings

### Information Sessions

This session introduces the audience to Voices for Children, and to the Child Advocacy Model. This information session is perfect for college students, community partners, and volunteers. (1 hour)

### Church/ Faith Communities - Keeping Children Safe Together

This training is specific for churches, temples, and places of worship, and includes information from several available trainings including Infant Safety, Preventing Child Sexual Abuse, Keeping Children Safe, mandated reporting, and more. (2.5 hours)

### Infant Safety

This training equips caregivers of infants with information regarding infant safe sleep, coping with developmental increased crying (PURPLE crying), and preventing child abuse related to infant crying, and caregiver stress/ fatigue.

Learn more about [Infant Safety on our Prevention page.](https://www.voicesforcac.org/prevention/)

### Mandated Reporting of Child Maltreatment

Mandated reporters are vital to the protection of children, especially during times of social distancing and increased remote/ virtual learning. Learn the signs of abuse and neglect and how to report your suspicions.

### Understanding the Impact of Childhood Trauma

This training explores ACEs (Adverse Childhood Experiences) and the lasting impact childhood trauma has on mental, and physical health. It will also examine ways to build on resiliency to improve lifetime health outcomes for youth facing adversity. This training is intended for youth-serving professionals, and parents/ caregivers of children and teens.

### ACEs & Protective Factors (for teens ages 11-17)

Much like Understanding the Impact of Childhood Trauma, this training discusses ACEs impact on health outcomes and builds on skills that protect and heal youth from adversity and trauma. This is intended for teen audiences, 11 - 17 years old.

### Preventing Child Sexual Abuse

The [_Stewards of Children: Darkness to Light_](https://www.d2l.org/education/stewards-of-children/) training prepares adults for noticing signs, responding to disclosures, and equipping your organization to prevent child sexual abuse.

Learn more [on our Prevention page.](https://www.voicesforcac.org/prevention/)

### LGBTQIA+ Youth Advocacy

An intro-level look at the most common vocab and identities in the LGBTQIA+ community. We'll walk through understanding the difference between sex, gender identity, gender expression, and attraction. We'll also explore health outcomes and disparities of queer youth, and look intersectionally at the specific ways queer people of color face adversity. This training examines minority stress and resilience factors, the critical role of family acceptance work, and ways to create safe and inclusive spaces in your place of work/ worship.

### Clinical Advocacy for Transgender & Non-Binary Youth

This is a deeper look into advocacy for Transgender & Non-Binary (TGNB) youth. This training covers the various ways TGNB youth socially, and/or physically transition, how structural cisheterosexism impacts minority stress, national trends in legislation, gender dysphoria, and more. This training is intended for youth-serving professionals. and caregivers of TGNB or questioning youth.

### Helping Kids Stay Safe in a Virtual World

This training examines how human trafficking functions, signs to look for in recognizing trafficking - including force, fraud, and coercion. This training also provides information all caregivers need to protect their children online.

Learn more about [Internet Safety on our Prevention page.](https://www.voicesforcac.org/prevention/)

### Talking to Children About Racism

Discrimination and prejudice deeply impact the health and well-being of black & indigenous people of color. Talking to children about racism can feel intimidating because there is so much painful history to unpack. You may be navigating your own racial trauma making it hard to talk meaningfully and openly with your child. You may be working through feelings of guilt, complicity, and shame. You may just not feel confident teaching your child about diversity, racism, and equity. In this training, we will examine systemic racism, white supremacy and privilege, microaggressions, intersectionality, and more.

### R.O.A.R. Body Safety (Kindergarten-Grade 2)

Voices for Children uses the R.O.A.R Educational Program to teach children how to protect themselves from harm. R.O.A.R. stands for:

* **R**-emember privates are private
* **O**-kay to say no
* **A**-lways talk about secrets
* **R**-aise your voice and tell someone.

This 25-minute interactive training is available to K and 2nd-grade students. Training takes place in the classroom, with parental permission. Parent support is also provided as a part of the curriculum so adults from the family can participate and practice this invaluable lesson, using common language, with their child at home.

Learn more about [R.O.A.R on our Prevention page.](https://www.voicesforcac.org/prevention/)

### Need Something Different?

If you are part of a youth-serving organization that wants to schedule a unique training experience for staff and volunteers, reach out to [training@voicesforcac.org](mailto:training@voicesforcac.org) to collaborate on ways we can partner and meet your needs.
