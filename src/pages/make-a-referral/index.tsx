import React from 'react'
import { Router } from '@reach/router'

import Layout from '../../components/layout'
import MakeAReferralGeneseeCounty from '../../components/app/make-a-referral-genesee-county'
import MakeAReferralShiawasseeCounty from '../../components/app/make-a-referral-shiawassee-county'
import LoginGeneseeCounty from '../../components/app/login-genesee-county'
import LoginShiawasseeCounty from '../../components/app/login-shiawassee-county'
import PrivateRoute from '../../components/app/private-route'
import MakeAReferral from 'src/components/app/make-a-referral'

const Private = () => {
  return (
    <Layout>
      <Router>
        <PrivateRoute
          county="genesee"
          path="/make-a-referral/genesee-county"
          component={MakeAReferralGeneseeCounty}
        />
        <PrivateRoute
          county="shiawassee"
          path="/make-a-referral/shiawassee-county"
          component={MakeAReferralShiawasseeCounty}
        />
        <LoginGeneseeCounty path="/make-a-referral/genesee-county/login" />
        <LoginShiawasseeCounty path="/make-a-referral/shiawassee-county/login" />
        <MakeAReferral path="/make-a-referral" />
      </Router>
    </Layout>
  )
}

export default Private
