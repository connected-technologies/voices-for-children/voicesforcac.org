---
template: page_with_blocks
title_background: Orange
title_background_image_opacity: 1
position: Above
title: Grievance
subtitle: ''
title_background_image: "/media/kristin-brown-3kBtYrZ0-hw-unsplash.jpg"
blocks: []

---
# Client Grievances

Voices for Children seeks to treat all clients with dignity, fairness, respect, and professionalism, and to strive for excellence in providing services to clients. Voices for Children's policy provides clients, and their families or legal guardians, with the opportunity to express any issues or grievances related to the quality of services provided.  If you feel you have been treated unfairly, or unprofessionally, or feel that your rights have been breached, please complete the form below to inform Voices for Children's staff and leadership.

The following options are available to you in the event of any service issues/conflict:

• If you are dissatisfied with Voices for Children's services, you can discuss the issue directly with your provider.

• If you are not satisfied with the solution reached after speaking with your provider about your concerns, if you feel that you have not been heard, or if you are not comfortable talking directly with your provider, you can:

1\. Contact our Vice President of CAC Services, and Compliance Officer, Ellen Lynch directly at ellen@voicesforcac.org, (989) 723-5877 ext.220.

1. Complete the form below to file a formal grievance. This grievance will be reviewed by our Vice President of CAC Services, and Compliance Officer, Ellen Lynch, ellen@voicesforcac.org, (989) 723-5877 ext.220. You may remain anonymous, or include your contact information to receive follow-up on what actions are taken to resolve the issue or complaint.

<a href="https://docs.google.com/forms/d/e/1FAIpQLScyk4-x7wiyX3RRSde8e7xg1X3E03LQRkYTeo2aQHD4vikeWA/viewform" class="button is-primary" target="_blank">Grievance Reporting Form</a>

3\. Write and submit a printed version of the grievance to our dropbox located in the family room at either the Shiawassee or Genesee County locations, or by mail to 1216 W Main St. Owosso, MI 48867. This form can be downloaded below, or completed in person during your appointment. Ask a staff person for a form if needed.

<a href="https://voicesforcac.org/media/client-grievance-reporting-form-print-ver8-10-22.pdf" class="button is-primary" target="_blank">Print Form</a>

**No individual, organization, or agency, may discharge or retaliate in any manner against any person that has filed a complaint or grievance.**

# Non-Discrimination Notice & Equal Opportunity

Voices for Children Advocacy Center, and its service providers, are dedicated to providing safe, consistent, and culturally informed services. Voices for Children does not discriminate against any individual seeking services on the basis of race, citizenship/status, color, religion, sex, national origin, age, orientation, disability, political affiliation, or beliefs.

If you believe that Voices for Children has failed to provide these services, or discriminated in another way, on the basis of race, citizenship/status, color, religion, sex, national origin, age, orientation, disability, political affiliation, or beliefs, You may submit your complaint to the local Equal Opportunity Officer:

Mr. Danis Russell, Voices for Children Board Member  
420 W 5th Ave, Flint, MI 48503  
(810) 257-3705  
Email: [drussell@genhs.org](mailto:drussell@genhs.org)

Or you may file a complaint within 180 days of the alleged incident with:

The Michigan Department of Civil Rights - Detroit Office  
Cadillac Place, 3054 West Grand Boulevard, Suite 3-600  
Detroit, MI 48202  
Toll-Free phone number: (800) 482-3604  
Email: [MDCRServiceCenter@michigan.gov](mailto:MDCRServiceCenter@michigan.gov)

**No individual, organization, or agency, may discharge or retaliate in any manner against any person that has filed a complaint, instituted any proceeding, testified, or is about to testify, in any proceeding or investigation, or has provided information or assisted in an investigation.**