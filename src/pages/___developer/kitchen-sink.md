---
title: Kitchen Sink (Title)
subtitle: "(Subtitle)"
template: page_with_blocks
blocks:
- template: block__hero
  component: hero
  background: Purple
  title: BLOCK__HERO (Title)
  subtitle: Optional background color or image (Subtitle)
  background_image: ''
  background_image_opacity: 
- template: block__3col
  component: 3col
  title: BLOCK__3COL (Title)
  col1:
    image: "/media/hope.jpg"
    title: Column One
    content: Here is some content.
    component: ''
    width: ''
    alignment: ''
  col2:
    image: "/media/hope.jpg"
    title: Column two
    content: Here is some content.
    component: ''
    width: ''
    alignment: ''
  col3:
    title: Column 3
    content: Here is some content.
    component: ''
    width: ''
    alignment: ''
- template: block__gallery
  component: gallery
  title: BLOCK__GALLERY
  description: This is a (Description)
  images:
  - image: "/media/hope.jpg"
    title: Group Photo (Title)
  - image: "/media/hope.jpg"
    title: Blue Pinwheels (Title)
  - image: "/media/hope.jpg"
    title: Paint the Town Outdoors (Title)
- template: block__feature
  component: feature
  background: Blue
  orientation: normal
  title: BLOCK__FEATURE (Title)
  content: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent auctor
    nunc id nunc fringilla, quis efficitur augue volutpat. Mauris accumsan, ante imperdiet
    bibendum euismod, ex sem molestie est, vitae pulvinar ante justo ut quam. Cras
    blandit erat a nibh cursus porta. Aenean a erat in ligula ullamcorper consequat.
    Vestibulum accumsan quam eros, eget aliquam est vulputate at. Etiam sollicitudin
    turpis nulla, ac porttitor urna auctor at. Curabitur sit amet tellus nisl.
  featured_content: <iframe title="featured-content-frame" width="560" height="315"
    src="https://www.youtube.com/embed/2CCNswShJRc" frameborder="0" allow="accelerometer;
    autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  background_image: ''
  image: ''
  background_image_opacity: 
- template: block__feature
  component: feature
  background: Dark
  orientation: reverse
  title: BLOCK__FEATURE (Title)
  content: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent auctor
    nunc id nunc fringilla, quis efficitur augue volutpat. Mauris accumsan, ante imperdiet
    bibendum euismod, ex sem molestie est, vitae pulvinar ante justo ut quam. Cras
    blandit erat a nibh cursus porta. Aenean a erat in ligula ullamcorper consequat.
    Vestibulum accumsan quam eros, eget aliquam est vulputate at. Etiam sollicitudin
    turpis nulla, ac porttitor urna auctor at. Curabitur sit amet tellus nisl.
  image: "/media/hope.jpg"
  featured_content: ''
  background_image: ''
  background_image_opacity: 
- template: block__feature
  component: feature
  background: Green
  orientation: reverse
  title: BLOCK__FEATURE (Title)
  content: Aenean a erat in ligula ullamcorper consequat. Vestibulum accumsan quam
    eros, eget aliquam est vulputate at. Etiam sollicitudin turpis nulla, ac porttitor
    urna auctor at. Curabitur sit amet tellus nisl.
  image: "/media/voices-for-children-logo.png"
  featured_content: ''
  background_image: ''
  background_image_opacity: 
- template: block__feature
  component: feature
  background: Light
  orientation: normal
  title: BLOCK__FEATURE (Title)
  content: Mauris accumsan, ante imperdiet bibendum euismod, ex sem molestie est,
    vitae pulvinar ante justo ut quam. Cras blandit erat a nibh cursus porta. Aenean
    a erat in ligula ullamcorper consequat.
  image: "/media/ccba-22.jpg"
  featured_content: ''
  background_image: ''
  background_image_opacity: 
- template: block__feature
  component: feature
  background: Orange
  background_image_opacity: 1
  orientation: reverse
  title: BLOCK__FEATURE (Title)
  content: "**Lorem ipsum dolor sit amet, consectetur adipiscing** elit. Praesent
    auctor nunc id nunc fringilla, quis efficitur augue volutpat. Mauris accumsan,
    ante imperdiet bibendum euismod, ex sem molestie est, vitae pulvinar ante justo
    ut quam. Cras blandit erat a nibh cursus porta. Aenean a erat in ligula ullamcorper
    consequat. Vestibulum accumsan quam eros, eget aliquam est vulputate at. Etiam
    sollicitudin turpis nulla, ac porttitor urna auctor at. Curabitur sit amet tellus
    nisl."
  image: "/media/robert-collins-lP_FbBkMn1c-unsplash.jpg"
  featured_content: ''
  background_image: ''
- template: block__content
  component: content
  alignment: Left
  content: |-
    ## BLOCK__CONTENT

    This is a content block

    ![Voices for Children](/media/voices-for-children-logo.png)

    _As you can see, it can also display **images**!_

    But can it use files? [Download this cool PDF](/media/CVSCBrochureJune2011_358764_7.pdf)
- template: block__cta
  component: cta
  background: Light
  title: BLOCK__CTA (Title)
  subtitle: Optional background color or image (Subtitle)
  button:
    url: "/donate"
    text: Donate
  background_image: ''
  background_image_opacity: 
- template: block__cta
  component: cta
  background: ''
  title: BLOCK__CTA (Title)
  subtitle: This has an image background (Subtitle)
  button:
    url: "/how-you-can-help"
    text: How You Can Help
  background_image: "/media/joshua-eckstein-VAJEea9u6k8-unsplash.jpg"
  background_image_opacity: 0.25 
- template: block__cards
  component: cards
  cards:
  - image: "/media/voices-for-children-logo.png"
    title_top: "(Title Top)"
    title: "(Title)"
    subtitle: "(Subtitle)"
    content: Here is some content to go in the first card.
    link: "/what-we-do"
  - image: "/media/Elga.png"
    title_top: "(Title Top)"
    title: "(Title)"
    subtitle: "(Subtitle)"
    content: Here is some content to go in the third card.
    link: "/what-we-do"
  - image: "/media/sharon-mccutcheon-Vl0KHsz67kE-unsplash.jpg"
    title_top: "(Title Top)"
    title: "(Title)"
    subtitle: "(Subtitle)"
    content: Here is some content to go in the second card.
    link: "/what-we-do"
- template: block__columns
  component: columns
  title: BLOCK__COLUMNS (Title)
  description: "(Description)"
  columns:
  - template: block__column
    component: column
    alignment: Left
    content: Column One
    width: ''
  - template: block__column
    component: column
    alignment: ''
    content: Column Two
    width: ''
- template: block__content
  component: content
  content: |-
    ## BLOCK__CONTENT

    Showing casing a YouTube embed:

    <iframe title="content-frame" width="560" height="315" src="https://www.youtube.com/embed/2CCNswShJRc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
- template: block__gallery
  component: gallery
  title: BLOCK__GALLERY (Title)
  description: "(Description)"
  images:
  - title: Wine & dine
    image: "/media/wine group.jpg"
  - title: T-shirt
    image: "/media/mens-fit-shirt.jpg"
  - title: Superhero 5K Run
    image: "/media/superhero-run-logo-no-year.png"
- template: block__banner
  component: banner
  image: "/media/ccba-22.jpg"
  alt_text: BLOCK__BANNER alt text
- template: block__map
  component: map
  latitude: '43.0181182'
  longitude: "-83.6835679"
  popup_content: "#### Voices for Children Advocacy Center</h4>\n\n[515 East Street
    \ \nFlint, MI 48503](https://www.google.com/maps/place/515+East+St,+Flint,+MI+48503/@43.018537,-83.683602,16z/data=!4m5!3m4!1s0x88238218c69c6601:0xf211a626507a2df2!8m2!3d43.0185367!4d-83.6836024?hl=en-US)\n\n[810-238-3333](tel:+1-810-238-3333)"
position: Above
alt_text: ''
image: ''
title_background: Light Blue
title_background_image: "/media/sara-torda-ViUzwBK0Vrs-unsplash.jpg"
title_background_image_opacity: 0.25

---
This page uses the **Page With Blocks** template.

It has examples of all the different types of blocks you can add to a page. But these are just a few examples; there are endless ways to customize the blocks and organize them to build a page.

### Block Types: (H3)

* block__hero
* block__feature
* block__3col
* block__cta
* block__content
* block__columns
* block__gallery
* block__map
* block__cards
* block__banner
