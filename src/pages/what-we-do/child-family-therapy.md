---
template: page_with_blocks
title_background: Light Blue
title_background_image_opacity: 0.25
position: Above
title: Child & Family Therapy
subtitle: ''
title_background_image: "/media/joshua-eckstein-VAJEea9u6k8-unsplash.jpg"
blocks:
- template: block__columns
  component: columns
  columns:
  - template: block__column
    component: column
    width: One Half
    alignment: Left
    content: "#### **Genesee County:**\n\n**Autumn Heddy-Reed, LMSW, MSW, MEd, ATR-BC**
      \ \nClinical Co-Director, Child & Family Therapist, Board Certified Art Therapist
      \ \n[autumn@voicesforcac.org](mailto:autumn@voicesforcac.org)\n\n**Kayla Bender,
      LMSW**  \nClinical Co-Director, Child & Family Therapist  \n[kayla@voicesforcac.org](mailto:kayla@voicesforcac.org)\n\n**Alonna
      Tipton, LPC**  \nChild & Family Therapist, Clinical Supervisor  \n[alonna@voicesforcac.org](mailto:alonna@voicesforcac.org)\n\n**Amanda
      Richard, LMSW**  \nFamily Advocate, Child & Family Therapist  \n[amanda@voicesforcac.org](mailto:amanda@voicesforcac.org)\n\n**Sonja
      Coleman, LLMSW**  \nChild & Family Therapist  \n[sonja@voicesforcac.org](mailto:sonja@voicesforcac.org)\n\n**Tabitha
      Neff**  \nCrisis Counselor  \n[tabitha@voicesforcac.org](mailto:tabitha@voicesforcac.org)"
  - template: block__column
    component: column
    alignment: Left
    content: "#### **Shiawassee County:**\n\n**Sierra Holtz, LLPC**  \nCrisis Counselor
      \ \n[sierra@voicesforcac.org](mailto:sierra@voicesforcac.org)"
    width: One Half
  description: Our therapy staff is comprised of caring, dedicated, individuals that
    are highly-trained, skilled professional therapists.
  title: Therapy Staff by County
- template: block__content
  component: content
  content: "We also Partner with local mental health organizations, Genesee Health
    System, and Mott Children's Health Center for referrals.\n\n**Genesee Health System**
    \ \n420 W 5th Ave,  \nFlint, MI 48503  \n(810) 257-3705  \n[http://www.genhs.org/](http://www.genhs.org/
    \"http://www.genhs.org/\")\n\n**Mott Children's Health Center**  \n806 Tuuri Pl,
    \ \nFlint, MI 48503  \n(810) 767-5750  \n[https://www.mottchc.org/](https://www.mottchc.org/
    \"https://www.mottchc.org/\")"

---
## Child & Family Therapy Services

Voices for Children Advocacy Center is proud to offer child and family therapy services. Our therapists are trained in many different therapy models including; Trauma-Focused Cognitive Behavioral Therapy (TF-CBT), Trauma Play, Parent-Child Interaction Therapy (CIT), Art Therapy, Eye Movement Desensitization and Reprocessing (EMDR), Child-Parent Psychotherapy (CPP), and more. These models are all used to address the unique needs of the families we serve, regardless of what brings them into treatment at Voices for Children.

Our therapy staff/ personnel provide a treasure trove of tools and techniques to enhance the creativity and fun as you learn and heal from emotional, psychological, and physical traumas that have occurred in your families and communities.

* Animal Assisted Therapy
* Court preparation
* Creative expression
* Emotional expression
* Equine Therapy
* Experiential mastery
* Group Therapy
* Kinesthetics
* Mindfulness
* Play-based gradual exposure
* Psychoeducation
* Puppets/Role Play
* Relaxation
* Sand play/ Moving Stories
* Sand tray therapy
* Yoga
* And more!