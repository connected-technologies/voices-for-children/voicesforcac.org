---
title: Court Appointed Special Advocates (CASA)
template: page_with_blocks
position: Below
title_background: Light Blue
title_background_image: "/media/joshua-eckstein-VAJEea9u6k8-unsplash.jpg"
title_background_image_opacity: 0.25
blocks: []

---
<a href="https://docs.google.com/forms/d/e/1FAIpQLSfY5CDt3SDPLz5dCLuPr9UEE2sNwyUKyeXnbVNdefMjn4zUKA/viewform" class="button is-primary" target="_blank">CASA Request Form</a>

CASA volunteers are specially trained to work on behalf of children in foster care to ensure the children receive the care and services they need. The CASA representative’s only job is to look out for the child’s best interest, from the time they enter foster care until they are reunited with their family or placed for adoption.

## CASA

* Appear in court to speak for the child
* Visits child regularly
* Let the child know that someone is looking out for them
* A CASA volunteer works with only 3-4 children at a time, so that each one receives priority and special attention

**_“Parents are supposed to provide love and support. But when that doesn’t happen, someone needs to speak up and protect the children who otherwise remain victims.”_**

This is where our CASA workers come into play. CASA workers are often the only consistent face that a child sees while in care. Being a CASA is truly an honor and can change a child’s life. We always need volunteers in the CASA program. If you are interested please click here to apply.

## What is CASA?

CASA stands for Court Appointed Special Advocate; a non-profit organization that is a tax-exempt 501 (c) 3 charity that recruits, trains and supports volunteers to represent the best interests of abused and neglected children in the courtroom and other settings. We are one of 933 organizations that provide similar services in juvenile courts across the United States.

Each CASA organization is independent and is governed by a local board of directors. The national CASA organization provides support and coordinates information for all CASA chapters.

## How did the CASA movement begin?

In 1977, a Seattle juvenile court judge concerned about making drastic decisions with insufficient information conceived the idea of citizen volunteers speaking up for the best interest of abused and neglected children in the courtroom. From that first program has grown a network of almost 1,000 CASA and guardian ad litem programs that are recruiting, training and supporting volunteers in 49 states and the District of Columbia.

## What do CASA Volunteers do?

CASA volunteers listen first. Then they act. Volunteers are advocates for children (newborns to 18 years of age) who are in the juvenile court system due to allegations of abuse and neglect. These are the most vulnerable children in our city. The CASA volunteer gets to know the child by talking with everyone in that child’s life: parents and relatives, foster parents, teachers, medical professionals, attorneys, social workers, and others. They use the information they gather to inform judges and others of what the child needs and what will be the best permanent home for them. Independent research has demonstrated that children with a CASA volunteer are substantially less likely to spend time in long-term foster care and less likely to reenter care.

## Who can be a CASA volunteer?

You do not have to be a lawyer or social worker to be a volunteer. We welcome people from all walks of life. We are simply looking for people who care about children and have common sense. As a volunteer, you will be thoroughly trained and well supported by professional staff to help you through each case.

We provide a required 33-hour pre-service training course. Volunteers must be at least 21 years old, pass a thorough background check, have their own transportation, and the ability to dedicate an average of 15-20 hours per month to the casework.

# 

## How many children need CASA?

Each year, the Genesee County Juvenile Court identifies nearly 580 “at high risk” children who need a CASA volunteer. Last year CASA was able to provide volunteers to only 90 of these children.

# 

## How are children assigned to CASA?

CASA volunteers are appointed by the Genesee County juvenile court judges to watch over and advocate for children, to make sure they don’t get lost in the overburdened legal and social service system or languish in inappropriate groups or foster homes. The court notifies CASA and asks for a volunteer to be assigned. CASA lets the court know its capacity to accept cases. Because there are not enough CASA volunteers to represent all of the children in care, judges typically assign CASA volunteers to their most difficult cases. Volunteers stay with each case until it is closed and the child is placed in a safe, permanent home. For many abused children, their CASA volunteer will be the one constant adult presence in their lives. Currently, 95% of children who are deemed by the court to be at high risk do not have the benefit of a CASA volunteer.

## Why do we need CASA? Why don’t the grandparents or other family members just step in?

Connecting the child with a safe place to live next requires immense effort, investigation, and knowledge of the legal process. There are courageous single parents, grandparents, aunts, and uncles who are doing all they can to love the children left behind by irresponsible, ill-equipped, and/or incapable parents. CASA is called by the court for children who are not being helped by a strong extended family. If the child has such a capable extended family, CASA may not be needed. Our advocates thoroughly explore safe and permanent placement options and recommend what is best for the child. For the children, CASA is often the only way they will get to a safe home.

## How does CASA do the job of advocating?

Genesee County CASA currently has one advocate supervisor on staff responsible for overseeing the work of our volunteer advocates. A trained volunteer is assigned to each case. A volunteer typically handles one case at a time but cases may overlap for a short period. At the end of the case, the child is placed into a home. A typical case will take 10-12 months to completion, but can take longer in some instances. We ask for a one-year commitment from our volunteers because it is critical that the same volunteer advocates for the child throughout the case.

## How is CASA different from the Guardian Ad Litem?

CASA volunteers come from all walks of life and professions. They are trained to work directly with children and families and provide the court recommendations in the child’s best interest. A Guardian Ad Litem (GAL) in Michigan is a court-appointed attorney appointed that is paid to represent a child legally in the juvenile system. There is often overlap with CASA’s mission and the work of a GAL in finding the best course of action for a child. Typically, a GAL has a large caseload while CASA volunteers are appointed to one case at a time. In some states, where a GAL is not required to be an attorney, the programs are combined and a CASA volunteer is treated as the GAL. Michigan is a state that requires a GAL to be an attorney.

## Why isn’t CASA taking all of the case referrals that are made by the juvenile court?

Genesee County CASA is handling all of the children we can support with our current resources. To serve the needs of 580 children annually, we will need the following –

* Funding to hire 20 additional advocate supervisors trained in social work
* Up to 500 new volunteers
* Funding for additional training and office facilities

We have adopted an aggressive business plan to grow our resources so that we can meet 100% of the need of the abused and neglected children in Genesee County. Our additional funding will have to come from private donors and foundations.

[Click here to learn the Ways You Can Give.](http://casanashville.org/ways-to-give/)

## How can I get involved?

There are several ways you can support CASA’s mission:

* Become a CASA volunteer. You can make a lasting impact a child’s life by becoming a CASA volunteer. Call Volunteer Coordinator at 810-238-3333 ext. 211 or [complete this form.](https://docs.google.com/forms/d/e/1FAIpQLSedfjkkocB6LTNvx3rTaN1PMRoCr6w5rM7rBN3MI2Jd_UNNiQ/viewform?c=0&w=1)
* Become a donor to CASA. It roughly takes $1,200 to provide one child with a CASA volunteer for one year. It costs $200 to train one new CASA volunteer. We rely on the community’s support to keep our program running. [Click here and donate today.](/donate)
* Support CASA’s events. Volunteer your time planning or working at one of CASA’s annual events. Or attend an event with your friends and family. [A full listing of events is located here.](http://www.michigancasa.org/events.html)
* Check to see if your place of employment has a giving program or foundation. If they do, call 810-238-3333 ext. 206.
* Spread the word about CASA’s mission. Host a party and invite a CASA representative to speak. Invite CASA to a meeting at your work, organization, bible study group, or book club.

CASA is a national model that has been shown to improve outcomes for participating children in the foster care system. As an evidence-based model, CASA of Genesee County has specific inputs, activities, and outcomes which have been organized, below, into a logic model explaining the program methodology.

[Take a closer look at the logic model for CASA.](https://drive.google.com/file/d/1tYFB_k90P--qcJ5ywMBVk5cVYB14AvWw/view?usp=sharing)

**_For more information about CASA or to ask us a question, please call 810-238-3333._**

## Hear More

Listen to "[CASA: Court Appointed Special Advocate](https://open.spotify.com/episode/0E0ayxUNaWmGFvLkQPGCW1?go=1&utm_source=embed_v3&t=0&nd=1)" an episode of the "[Adoption Hacks](https://open.spotify.com/show/6FBwwVEtKFMKzMGq0zWpdo)" podcast, to hear more about CASA. 

"CASA supports and promotes court appointed volunteer advocacy so every child who has experienced abuse or neglect can be safe, have a permanent home and the opportunity to thrive in a safe and loving home.

If you are curious what CASA is, how it works, how it impacts foster kids and how to get involved - this is a great episode to listen to! Get all the info here!"

<iframe src="https://open.spotify.com/embed-podcast/episode/0E0ayxUNaWmGFvLkQPGCW1" width="100%" height="232" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
