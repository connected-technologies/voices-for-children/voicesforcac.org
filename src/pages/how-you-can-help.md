---
title: How You Can Help
template: blocks
blocks:
- template: block__cta
  component: cta
  background: Purple
  background_image: "/media/moses-vega-_YfoApRxd4I-unsplash.jpg"
  background_image_opacity: 0.25
  button:
    url: "/donate"
    text: Give Today
  title: Donate
  subtitle: Children need fearless, compassionate adults who will protect them from
    the perils of abuse and neglect. Thank you for your support.
- template: block__feature
  component: feature
  background: Blue
  orientation: reverse
  title: Volunteer Opportunities
  content: <ul> <li>Court Appointed Special Advocates</li> <li>Family Hosts</li> <li>Internships</li>
    <li>Office / Clerical</li> <li>Beautification / Gardening</li> <li>Ambassadors</li>
    <li>Tour Guides</li> </ul> <p> <a href="https://goo.gl/forms/e2KnMKvwUVwATTU33"
    class="button is-white is-outlined">Apply Now</a> </p>
  image: "/media/voices-for-children-staff-23.jpg"
  button:
    url: "/donate"
    text: Give Today
  featured_content: ''
  background_image: ''
  background_image_opacity: 
- template: block__columns
  component: columns
  title: Pantry of Love
  columns:
  - template: block__column
    component: column
    width: One Third
    alignment: Center
    content: '![Pantry of Love: providing personal care items for children in need](/media/pantry-of-love.jpg
      "Pantry of Love")'
  - template: block__column
    component: column
    width: Two Thirds
    alignment: Left
    content: |-
      The Pantry of Love provides new basic essentials to children and families who have experienced trauma. At Voices for Children, more than 2000 families a year receive hope, help, and healing from child sexual abuse, physical abuse, neglect, and child human trafficking.

      The Pantry of Love is a space where these children are provided with essential basic needs items such as toiletries, clothing, a clean set of sheets, a soft blanket for comfort, and a new pair of shoes and socks. Every item in our pantry is brand new. The reason for this is that we believe these children must feel highly valued and worthy. We show this by providing them with brand-new, nice items.

      **We want children to leave Voices for Children knowing they are worthy, they are valued, and they are loved. You can Help!**

      <a href="https://secure.givelively.org/donate/voicesforcac/pantry-of-love" class="button is-primary" target="_blank">Pantry of Love</a>

      If you would like to donate in-kind, check out some ideas below of things that would be really useful to us and the families we serve.
  description: ''
- template: block__columns
  component: columns
  columns:
  - template: block__column
    component: column
    width: One Quarter
    alignment: Left
    content: |-
      ![Photo of a basket filled with snacks including chips, gummy snacks, cookies, crackers, juice boxes, water, and more.](/media/snack-basket.jpg "Snack Basket")

      ## Snacks and Beverages

      Consider donating individually wrapped snacks, and beverages, that we can offer families when they come to the center for therapy and during forensic interviews. This goes a long way to make sure they feel comfortable, and to meet their needs: hungry kids can't focus or heal. Consider:

      * Juice Boxes
      * Water Bottles
      * Hot Cocoa Packets
      * Soda/ Pop
      * Keurig Coffee Pods
      * Individually Packed Chips (especially hot chips)
      * Fruit Snacks
      * Cookies
      * Pop Tarts
      * & More
  - template: block__column
    component: column
    width: One Quarter
    alignment: Left
    content: |-
      ![](/media/gift-cards.jpg)

      ## Gift Cards

      Gift card donations offer a ton of versatility in helping families' unique needs. These could be;

      * Food gift cards we can give to families when at court for long periods of time and the family is made to wait through lunch, like; Halo Burger, McDonald's, Taco Bell, Arby's, etc.
      * Chain store gift cards for room needs as listed before, or for grocery or clothing help after a change in a child's placement.
      * There are many situations where a gift card can go a long way in showing care for a family.
  - template: block__column
    component: column
    width: One Quarter
    alignment: Left
    content: |-
      ![](/media/hygiene.jpg)

      ## Hygiene & Care Packs

      Personal care items, and hygiene products, are really helpful to families and survivors. Though especially important for human trafficking victims who are often rescued from the scene and need immediate access to personal care, this care is important to all survivors in the aftermath of abuse.

      * Plastic Applicator Tampons / Pads
      * Toothbrushes / Toothpaste / Floss / Mouthwash
      * Soap/ Body Wash
      * Shampoo / Conditioner
      * Deodorant / Antiperspirant
      * Make-up Wipes
      * Lip Balm
      * Etc.
  - template: block__column
    component: column
    width: One Quarter
    alignment: Left
    content: "![](/media/black-hair-care.jpg)\n\n## BIPOC Kits\n\nWe are super grateful
      for all the hygiene products we receive but please consider donating products
      that specifically support BIPOC families' unique needs. Too often these products
      are overlooked and are used quickly, so we often run low.\n\n* Hair Moisture\n*
      Leave-in Conditioner\n* Edge Control\n* Black Hair Cleansing Products\n* Bonnets\n*
      Hair Ties / Charms / Adornments\n* Fringe-Smoother\n* and consider out-of-the-box
      items like books on black hair, body positivity, diversity, etc.\n\n#### "
  title: In Kind Donation Items / Gift Kits
  description: ''
- template: block__columns
  component: columns
  columns:
  - template: block__column
    component: column
    width: One Quarter
    alignment: Left
    content: |-
      ![](/media/court-fidgets.jpg)

      ## Court Support Packs

      Our team of family advocates and Child & Family Therapists help families by accompanying them to court when a child has to testify. While there, families spend a lot of time waiting. It can be stressful and often, kids feel nervous. Having fidget toys, focus items like coloring books, and other comfort items go a long way in helping them feel more at ease.

      * Stress Balls
      * Fidget Toys
      * Spinners
      * Pop-its
      * Origami Paper
      * Coloring Books
      * Journals
      * Pens
      * Colored Pencils
      * Markers
      * Travel Tissue Packs
      * Etc.
  - template: block__column
    component: column
    width: One Quarter
    alignment: Left
    content: |-
      ![](/media/room-makeover.jpg)

      ## Room Makeover Packages

      In the aftermath of abuse, having a change in the child's environment can be incredibly healing - especially if the abuse took place in that child's bedroom. Oftentimes, moving is just not financially feasible for families. Giving them the power to change things around/ give new life to spaces can help kids feel more comfortable, and give some psychological distance to the environment of their abuse.

      * New Bedding / Comforters / Sheets
      * New Pillows / Pillow Cases
      * Decorative Throw Pillows
      * Gift Cards to various chain stores like Walmart, Target, Kohl's, Meijer, TJMaxx, Home Goods, etc.
  - template: block__column
    component: column
    content: |-
      ![](/media/family-room-daphne.jpg)

      ## Onsite Kits

      Help children and families feel comfortable when they come to Voices for Children for interviews, and therapy appointments. Show care with these small gestures that go a long way, and with supplies for staff to keep play areas clean.

      * Fidget Toys
      * Markers (Washable)
      * Paper
      * Coloring Books
      * Crayons
      * Colored Chalk
      * Cleaning Supplies (Clorox wipes, sanitizer, etc.)
    width: One Quarter
    alignment: Left
  title: ''
  description: ''
alt_text: ''
position: Above
subtitle: ''
title_background: ''
title_background_image: ''
title_background_image_opacity: 1

---
