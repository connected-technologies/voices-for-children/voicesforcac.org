---
template: page_with_blocks
title_background: Light Blue
title_background_image_opacity: 0.25
position: Above
title: Endowment
subtitle: ''
title_background_image: "/media/kristin-brown-3kBtYrZ0-hw-unsplash.jpg"
blocks:
- template: block__content
  component: content
  content: |-
    ### We have never been more scared for the children we serve...

    In the last few weeks, alone, the sustainability of our services and the critical work of Voices for Children was significantly threatened. The State of Michigan is, and has historically been, our largest funder for these life-saving services, and they suddenly and without adequate notice reduced our funding by 67%. That is a reduction of $750,000. We were shocked, alarmed, dismayed, but most of all scared. As we advocated voraciously to restore our funding, we watched 57 children come to the center for healing services in just **one week**. Nearly _sixty_ children needed us and our funding was in danger!

    We thought about what our community would look like without Voices for Children:

    * Charlie, a 7 yr old, would not receive free play therapy to heal from the negative impact of the sexual abuse he suffered from his uncle
    * Mari, 14 yrs old, who attempted suicide after being raped at a party, would not be smiling as she greeted her therapist and worked on her art project
    * Kylie, a 4-year-old who was excited to come to the Big Blue House for the first time and just wanted to play with Daphne, our canine companion, would have suffered even worse than the broken arm and collarbone from her abusive parents
    * Terri and Kerri, tween siblings, who are survivors of human trafficking and working with their therapist to prepare to testify in court against their trafficker, would be scared and alone, trying to navigate complex systems not designed to be for the child’s best interest.

    This list of names and stories of these children and teens could go on for pages. Think about that for a moment. This list represents just 5 of 57 children seen in one week at Voices for Children.

    What we realize, in thinking about a Genesee County without Voices for Children is that our services must remain available for children. These programs cannot be at the whims of often capricious funders.
- template: block__feature
  component: feature
  background_image_opacity: 1.0
  orientation: normal
  title: You can help us. You can help them.
  content: |-
    Voices for Children, with your support, wants to secure our future and ensure we can continue being a voice for children in our community. This way, our kids have somewhere safe to come for HOPE, HELP, and HEALING long into the future. With your contribution, we can wisely invest in this vision and step closer to self-sustainability.

    Donate today and choose our endowment option to grow your investment.

    <a href="https://secure.givelively.org/donate/voicesforcac/endowment" class="button is-primary" target="_blank">Donate Today</a>
  image: "/media/charlie-play-therapy-2.jpg"
  featured_content: ''
  background: Light Blue
  background_image: ''

---
