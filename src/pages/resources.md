---
title: Resources
template: page_with_blocks
title_background: Red
title_background_image: "/media/kristin-brown-3kBtYrZ0-hw-unsplash.jpg"
title_background_image_opacity: 0.25
blocks:
- template: block__cards
  component: cards
  cards:
  - image: ''
    title_top: ''
    title: Reporting Abuse
    subtitle: ''
    content: It's our collective responsibility to protect children in our community.
      Learn more about making a report here.
    link: "/resources/reporting"
  - title_top: ''
    title: FAQs
    subtitle: ''
    content: Answers to our most common questions
    link: "/resources/faqs"
    image: ''
  - image: ''
    title_top: ''
    title: Your Appointment
    subtitle: ''
    content: 'Your safety is our priority. Voices for Children is taking several safety
      measures in order to protect families and staff, and slow the spread of COVID-19
      in our community. If you are coming in for a forensic interview, here is what
      you can expect:'
    link: resources/your-appointment
  - image: ''
    title_top: ''
    title: Social Stories
    subtitle: ''
    content: Prepare your child for their visit  to Voices for Children
    link: "/resources/social-stories"
  - image: ''
    title_top: ''
    title: Resource Library
    subtitle: ''
    content: Empowering families with access to support materials and education.
    link: "/resources/resource-library/"
  - title_top: ''
    title: Crime Victims Compensation
    subtitle: ''
    content: Support for families in need
    link: "/resources/crime-victims-compensation"
    image: ''
  - image: ''
    title_top: ''
    title: Earned Income Tax Credit
    subtitle: ''
    content: Earned Income Tax Credit, (EITC), is an important benefit providing extra
      money to working families. To claim EITC, you need to meet certain requirements
      and file a tax return, even if you have no other filing requirement or owe no
      tax.
    link: "/resources/earned-income-tax-credit"
  - image: ''
    content: If you are a client of Voices for Children, and feel you have been treated
      unfairly, unprofessionally, or feel that your rights have been breached, read
      more, and file a grievance so that we can better serve clients and resolve any
      issues.
    link: ''
    title: Report a Greivance
    title_top: ''
    subtitle: ''
position: Above
alt_text: ''
subtitle: ''

---
