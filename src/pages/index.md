---
title: Homepage
subtitle: Page Subtitle
template: blocks
blocks:
- template: block__cta
  component: cta
  background: Dark
  background_image: "/media/laugh-kid.jpg"
  title: Voices for Children
  subtitle: ''
  button:
    url: "/how-you-can-help"
    text: Learn how you can help
  background_image_opacity: 1
- template: block__content
  component: content
  content: |-
    Our center serves as a voice and an advocate for children throughout Genesee and Shiawassee Counties. Voices for Children Advocacy Center serves  (1) child survivors of abuse and neglect and their families (including human trafficking), (2) with prevention for children in Genesee and Shiawassee Counties, and (3) advocacy for the well-being of the child.

    Voices for Children Advocacy Center works to intervene the alarming number of children who are survivors of abuse by educating, empowering, and enlightening the community, school-aged children, and parents about prevention factors. Voices for Children leads the community in responding to victims of child abuse violence and works to break the cycle of abuse via prevention, and intervention programming, as well as community education. Voices for Children is dedicated to building healthy relationships and provides a continuum of services for persons of all ages who have been, or are at risk of being, victims of child abuse or neglect. Our agency’s model is grounded in a strengths-based, empowerment philosophy and incorporates best practices and evidence-based strategies.

    All of our services are completely free to the family. Voices for Children Advocacy Center creates a vital link between community services and survivors of child abuse.

    ## Our Mission

    Voices for Children Advocacy Center is dedicated to serving child survivors of abuse, and their families, in Genesee and Shiawassee Counties and enhancing the lives of all children through treatment, education, and by increasing community awareness.

    ## Our Vision

    Shining a light so all children are empowered, and all families are thriving.

    ## Our Philosophy

    Voices for Children is rooted in a philosophy of empowerment and is actively promoting the growth and success of survivors of child physical and sexual abuse, human trafficking, neglect, and witness to violence. The center is working to mitigate power imbalances between survivors and abusers. This is accomplished through partnership, warm-handoff referrals for additional resources, and education. Voices for Children's clients are treated with respect and services are delivered in thoughtful and intentional ways that uphold survivor dignity. Barrier reduction is critical to ensuring survivors successfully connect with the resources and services necessary to make impactful and long-term changes. Systemic change is achieved as Voices for Children partners and brings together a multidisciplinary team of stakeholders to assess and improve systems impacting survivors.
- template: block__feature
  component: feature
  background_image_opacity: 1
  orientation: reverse
  title: Amp up their Voices
  content: |-
    Human Trafficking, child sexual abuse, and exploitation are painful realities to face, and far too common across Genesee and Shiawassee counties. Voices for Children is dedicated to giving survivors high-quality services on their path to healing and justice. With contributions from everyday heroes like you, we can amplify the voices of these survivors. Make a difference with your donation today.

    <a href="https://voicesforcac.org/donate/" class="button is-primary" target="_blank">Donate</a>
  image: ''
  background: Purple
  background_image: ''
  featured_content: <iframe title="Thank you heroes, give to Voices for Children"
    width="560" height="315" src="https://www.youtube.com/embed/J9uMEpJNInA" frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen></iframe>
- template: block__columns
  component: columns
  columns:
  - template: block__column
    component: column
    width: One Half
    alignment: Center
    content: "![](/media/mens-fit-shirt.jpg)"
  - template: block__column
    component: column
    width: One Half
    alignment: Left
    content: |-
      ## Support Voices for Children with Swag!

      Get ready for this years' Wear Blue Day, April 1, 2021, with our Voices for Children Swag - Every Child Matters. Head to our store to get your T-shirts or hoodies today!

      <a href="https://shop.voicesforcac.org/" class="button is-primary" target="_blank">Voices for Children Store</a>

      Wear Blue Day raises awareness of the prevalence of child abuse and neglect while honoring victims, survivors, and those we've lost.
  title: ''
  description: ''
- template: block__columns
  component: columns
  title: Updated Resource Library!
  columns:
  - template: block__column
    component: column
    width: Two Thirds
    alignment: Left
    content: "Browse over 500 titles in Voices for Children's updated resource library,
      available to families receiving services, as well as our multidisciplinary team.
      This collection is dedicated to empowering families with access to support and
      education. Visit our library and search for topics including abuse, body safety,
      diversity & inclusion, grief & loss, and much more. Call our family advocates
      to learn more or reserve a book today. \n\n<a href=\"https://www.librarycat.org/lib/voicesforchildren\"
      class=\"button is-primary\" target=\"_blank\">Resource Library</a>"
  - template: block__column
    component: column
    width: One Third
    alignment: Center
    content: <div id="w3e39be25a598a13e0eabf78f9a47b1d2"></div><script type="text/javascript"
      charset="UTF-8" src="https://www.librarything.com/widget_get.php?userid=voicesforchildren&theID=w3e39be25a598a13e0eabf78f9a47b1d2"></script><noscript><a
      href="https://www.librarything.com/profile/voicesforchildren">My Library</a>
      at <a href="https://www.librarything.com">LibraryThing</a></noscript>
  description: ''
- template: block__content
  component: content
  content: "## Non-Discrimination Notice & Equal Opportunity\n\nVoices for Children
    Advocacy Center, and its service providers, are dedicated to providing safe, consistent,
    and culturally informed services. Voices for Children does not discriminate against
    any individual seeking services on the basis of race, citizenship/status, color,
    religion, sex, national origin, age, orientation, disability, political affiliation,
    or beliefs.\n\nIf you believe that Voices for Children has failed to provide these
    services, or discriminated in another way, on the basis of race, citizenship/status,
    color, religion, sex, national origin, age, orientation, disability, political
    affiliation, or beliefs, You may submit your complaint to the local Equal Opportunity
    Officer:\n\nMr. Danis Russell, Voices for Children Board Member  \n420 W 5th Ave,
    Flint, MI 48503  \n(810) 257-3705  \nEmail: [drussell@genhs.org](mailto:drussell@genhs.org)\n\nOr
    you may file a complaint within 180 days of the alleged incident with:\n\nThe
    Michigan Department of Civil Rights - Detroit Office  \nCadillac Place, 3054 West
    Grand Boulevard, Suite 3-600  \nDetroit, MI 48202  \nToll-Free phone number: (800)
    482-3604   \nEmail: [MDCRServiceCenter@michigan.gov](mailto:MDCRServiceCenter@michigan.gov)\n\nNo
    individual, organization, or agency, may discharge or retaliate in any manner
    against any person that has filed a complaint, instituted any proceeding, testified,
    or is about to testify, in any proceeding or investigation, or has provided information
    or assisted in an investigation."
- template: block__feature
  component: feature
  background: Blue
  orientation: reverse
  title: Meet Daphne - Canine Advocate
  content: |-
    Say hello to Daphne! She may be the quietest member of our team, but she sure has the loudest “voice” when it comes to bringing comfort to the people we work with in the community. She is our Canine Companion and is so much more than just an adorable 5 year old Black Labrador/Golden Retriever hybrid dog. Daphne is a smile-maker, sadness-taker, joyful-spirit and fearless-friend who loves giving kisses, chasing squirrels, and hunting for sticks.

    Daphne joined Voice for Children in 2017 to accompany children in forensic interviews, support groups, court proceedings, medical exams and other events throughout the community. She has touched many lives, comforted several children, and brings incredible happiness to everyone she meets. Be sure to visit with Daphne when you visit Voices for Children. You won't regret it!
  featured_content: <iframe title="Meet Daphne" width="560" height="315" src="https://www.youtube.com/embed/r9HYNJwjMZc"
    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen></iframe>
  background_image: ''
  background_image_opacity: 
  image: ''
position: Above
alt_text: ''

---
