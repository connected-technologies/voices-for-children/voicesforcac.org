---
template: page_with_blocks
title_background: Orange
title_background_image_opacity: 0.25
position: Above
title: Advisory Board
subtitle: ''
title_background_image: "/media/mi-pham-FtZL0r4DZYk-unsplash.jpg"
blocks:
- template: block__cards
  component: cards
  cards:
  - image: ''
    title_top: ''
    title: Laura Jasso
    subtitle: Hurley Medical Center
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Robert Ruppel
    subtitle: Remax Platinum - Grand Blanc
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Polly Sheppard
    subtitle: Sheppard Consulting
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Gail Stimson
    subtitle: Stimson Consulting
    content: ''
    link: ''

---
