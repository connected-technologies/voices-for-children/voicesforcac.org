---
title: Overview & Mission
template: page_with_blocks
position: Below
title_background: Orange
title_background_image: "/media/mi-pham-FtZL0r4DZYk-unsplash.jpg"
title_background_image_opacity: 0.25
blocks: []

---
## Overview

Our center serves as a voice and an advocate for children throughout Genesee and Shiawassee Counties. Voices for Children Advocacy Center serves (1) child victims of abuse and neglect and their families (including human trafficking), (2) with prevention for children in Genesee and Shiawassee Counties, and (3) advocacy for the well-being of the child.

Voices for Children Advocacy Center works to intervene the alarming number of children who are victims of abuse by educating, empowering, and enlightening the community, school-aged children, and parents about prevention factors. Voices for Children leads the community in responding to victims of child abuse violence and works to break the cycle of abuse via prevention, and intervention programming, as well as community education. Voices for Children is dedicated to building healthy relationships and provides a continuum of services for persons of all ages who have been, or are at risk of being, victims of child abuse or neglect. Our agency’s model is grounded in a strengths-based, empowerment philosophy and incorporates best practices and evidence-based strategies.

All of our services are completely free to the family. Voices for Children Advocacy Center creates a vital link between community services and victims of child abuse.

## Our Mission

Voices for Children Advocacy Center is dedicated to serving the child victims and families of child abuse in Genesee and Shiawassee Counties and enhancing the lives of all children through treatment, education, and by increasing community awareness.

## Our Vision

Shining a light so all children are empowered, and all families are thriving.

## Our Philosophy

Voices for Children is rooted in a philosophy of empowerment and is actively promoting the growth and success of survivors of child physical and sexual abuse, human trafficking, neglect, and witness to violence. The center is working to mitigate power imbalances between victims and abusers. This is accomplished through partnership, warm-handoff referrals for additional resources, and education. Voices for Children's clients are treated with respect and services are delivered in thoughtful and intentional ways that uphold survivor dignity. Barrier reduction is critical to ensuring survivors successfully connect with the resources and services necessary to make impactful and long-term changes. Systemic change is achieved as Voices for Children partners and brings together a multidisciplinary team of stakeholders to assess and improve systems impacting victims.