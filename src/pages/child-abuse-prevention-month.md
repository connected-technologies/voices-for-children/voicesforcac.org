---
template: page_with_blocks
title_background: Blue
title_background_image_opacity: 0.25
position: Above
title: Child Abuse Prevention Month
subtitle: ''
title_background_image: "/media/mi-pham-FtZL0r4DZYk-unsplash.jpg"
blocks:
- template: block__feature
  component: feature
  background_image_opacity: 1
  orientation: normal
  image: ''
  content: "Paint our Town Blue is a collaboration of Voices for Children and Whaley
    Children's center in partnership with Ennis and Boys and Girls Club.\n\nThere
    are over 1,400 confirmed cases of child abuse in our county every year. National
    Child Abuse Prevention Month recognizes the importance of families and communities
    working together to strengthen families to prevent child abuse and neglect.\n\nThrough
    this collaboration, prevention services and supports help protect children and
    produce thriving families.\n\nClick [here](https://www.voicesforcac.org/prevention/
    \"Prevention\") learn more about preventing child abuse in your community. "
  title: Paint the Town Blue
  background_image: ''
  background: Light Blue
  featured_content: ''
- template: block__columns
  component: columns
  title: ''
  description: ''
  columns:
  - template: block__column
    component: column
    content: "![](/media/blue-sunday.jpg)"
    alignment: Center
    width: One Half
  - template: block__column
    component: column
    content: |-
      # Blue Sunday is April 24, 2022

      Get your faith community or church engaged in Child Abuse Prevention with the upcoming Blue Sunday. A dedicated day for faith communities to pray, share awareness, volunteer, and give back, as well as learn more about the impact of child abuse.
    alignment: Left
    width: One Half

---
# April is Child Abuse Prevention Month

April is here! You can be a Voice for Children today.

<a href="https://www.voicesforcac.org/donate" class="button is-primary" target="_blank">Donate Today</a>

Visit our [virtual calendar](https://drive.google.com/file/d/1nQ7RdrQ50gzCaUYMoxVjd1SOGWwC97mf/view) for more ways you can support Child Abuse Prevention the entire month of April!

<a href="https://drive.google.com/file/d/1nQ7RdrQ50gzCaUYMoxVjd1SOGWwC97mf/view" class="button is-primary" target="_blank">Virtual Calendar</a>