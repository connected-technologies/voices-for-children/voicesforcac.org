---
template: page_with_blocks
title_background: Red
title_background_image_opacity: 0.25
position: Above
title: Reporting
subtitle: ''
title_background_image: "/media/kristin-brown-3kBtYrZ0-hw-unsplash.jpg"
blocks:
- template: block__columns
  component: columns
  columns:
  - template: block__column
    component: column
    width: One Half
    alignment: Left
    content: |-
      ### [**Abuse & Neglect**](https://www.michigan.gov/mdhhs/0,5885,7-339-73971_7119---,00.html "Report Abuse & Neglect")

      If you suspect abuse or neglect, call **855-444-3911** any time day or night. This toll-free phone number allows you to report abuse or neglect of any child or adult.

      One number. One call. One person can make a difference.

      If you are reporting an emergency call **911!**
  - template: block__column
    component: column
    width: One Half
    alignment: Center
    content: "[![Speak Up! Report Abuse - 855-444-3911](/media/report-abuse-web-postercps_393032_7-1.jpg)](https://www.michigan.gov/mdhhs/adult-child-serv/abuse-neglect/childrens)"
  title: ''
  description: ''
- template: block__feature
  component: feature
  background_image_opacity: 1
  orientation: normal
  image: "/media/protect quote.png"
  content: |-
    ### <a href="https://drive.google.com/file/d/1ZpeYzAPTrWWmcn7MhskT0s0qoPFpRp_O/view?usp=sharing" target="_blank">Children Need Adults to Report</a>

    ### <a href="https://drive.google.com/open?id=1mkGwSsS7Y-xi1OLeu7z1WD0gHmrZ9o1r" target="_blank">Genesee County Schools Reporting Resource</a>

    ### <a href="https://drive.google.com/open?id=1qs9gBekOCwxkYqLPU5mrxsLu03Wt8u5o" target="_blank">Shiawassee County Schools Reporting Resource</a>
  title: ''
  featured_content: ''
  background: ''
  background_image: ''

---
