---
title: Frequently Asked Questions
template: page_with_blocks
title_background: Red
title_background_image: "/media/kristin-brown-3kBtYrZ0-hw-unsplash.jpg"
title_background_image_opacity: 0.25
position: Below
blocks: []
subtitle: ''

---
### What is the Children’s Advocacy Center?

The CAC is a child-friendly facility that provides assessment, advocacy, and therapy services to abused and neglected children, all under one roof.

### What is a Forensic Interview?

A Forensic Interview is a non-leading objective interview completed to obtain accurate information from a child. Forensic interviews are conducted with children, adolescents, and handicapped adults who are victims of a crime or a witness to a crime.

### What is an Extended Forensic Interview?

Interviews are conducted by child-friendly, specially educated, and trained Forensic Interviewers who have experience and knowledge in child development, child abuse, and trauma issues, and the investigative process. It is our goal to obtain necessary information in a non-threatening manner.

An extended forensic interview is a multi-session structured interview. This model recognizes that some children may need more than one session to talk about allegations of abuse and increases the number of interview sessions with the child to as many as four. An extended forensic interview is generally considered for children with special developmental considerations or children who are particularly anxious or frightened.

### Why do I have to bring my child to a Children’s Advocacy Center?

The CAC’s goal is to reduce a child’s trauma by reducing the number of times a child has to be interviewed regarding allegations of abuse; creating a positive and culturally sensitive response to these allegations, and; linking the families to supportive services. The CAC is a neutral, child-friendly, and safe place.

### What does an advocate do?

Family advocates and Crisis Counselors provide immediate support and crisis intervention; education about the child welfare and legal systems involved in child abuse cases; information on how sexual abuse impacts children and families, and; the importance of obtaining treatment for children. Advocates also provide referrals to community resources, training for caregivers on how to best support their children, and advocacy for families.