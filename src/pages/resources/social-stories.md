---
template: page_with_blocks
title_background: Red
title_background_image_opacity: 0.25
position: Above
title: Social Stories
subtitle: ''
title_background_image: "/media/kristin-brown-3kBtYrZ0-hw-unsplash.jpg"
blocks:
- template: block__columns
  component: columns
  columns:
  - template: block__column
    component: column
    width: One Half
    alignment: Left
    content: "## Genesee County\n\n**Mya Mitchell**  \n[mya@voicesforcac.org](mailto:mya@voicesforcac.org)
      \ \n(810) 238-3333 ext. 213\n\n**Tabitha Neff, BSW**  \n[tabitha@voicesforcac.org](mailto:tabitha@voicesforcac.org)
      \ \n(810) 238-3333 ext. 212\n\n#### Social Stories\n\n[Young Girl](https://drive.google.com/file/d/17SvjMy6kDjMWGYfjtQbFGdG2DrXbD0Fr/view?usp=sharing)
      \ \n[Teen Girl](https://drive.google.com/file/d/17GNVm25_dCpunFfR5gatOPL3i31SBBPE/view?usp=sharing)
      \ \n[Young Boy](https://drive.google.com/file/d/1btmgdvQAZkMQ2YtUCh7VmzHAz0VahY9C/view?usp=sharing)
      \ \n[Teen Boy](https://drive.google.com/file/d/1wVwp2F56VCOHWAelb-uM0VhE1NAHEt7M/view?usp=sharing)
      \ \n[General Story](https://drive.google.com/file/d/15nVNMKx8UKW5cbo-HV1y-l4j9prshZD2/view?usp=sharing)"
  - template: block__column
    component: column
    content: "## Shiawassee County\n\n**Rachel Sieman**  \n[Rachel@voicesforcac.org](mailto:Rachel@voicesforcac.org)  \n(989) 723-5877 ext. 225\n\n#### Social Stories\n\n[General
      Story](https://drive.google.com/file/d/19O23klNQHdmX9h0lZQ3fdpiCHDCYy_r9/view?usp=sharing
      \"Shiawassee County Social Story\")"
    width: One Half
    alignment: Left
  title: ''
  description: ''
- template: block__gallery
  component: gallery
  images:
  - title: Entrance
    image: "/media/social-story-entrance.jpg"
  - title: Family Room
    image: "/media/social-story-family-room.jpg"
  - title: Young girl
    image: "/media/little-girl-social-story.jpg"
  - title: Young Boy
    image: "/media/img-1113.jpg"
  - title: Interview Room
    image: "/media/img-1088.jpg"
  - title: Teen girl
    image: "/media/teen-girl-social-story.jpg"
  - title: Teen boy
    image: "/media/teen-boy-social-story-2.jpg"
  title: ''
  description: ''

---
A social story serves to introduce incoming young survivors Voices for Children. These stories are especially helpful for youth with Autism Spectrum Disorder, or any type of learning or social impairment. Below, you will find social stories that may speak to the child's social identity that you can use to prepare them for their initial visit for a forensic interview. If there is anything that the staff at Voices for Children can do in order to accommodate and honor the needs of your child please contact our Family Advocates below.
