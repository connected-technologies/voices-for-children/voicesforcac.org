---
template: page_with_blocks
title_background: Blue
title_background_image_opacity: 0.25
position: Above
title: Capital Campaign
subtitle: ''
title_background_image: "/media/dv3-A.jpg"
blocks:
- template: block__columns
  component: columns
  columns:
  - template: block__column
    component: column
    alignment: Center
    width: One Half
    content: |-
      #### ![](/media/dv3-A.jpg)

      #### [Exterior Plans](https://drive.google.com/open?id=11rGoG5lKCNAm0RiEbBK2g4_aBp9QTXzK "Exterior Plans")

      #### [Floor Plan](https://drive.google.com/open?id=1Fm3R4BJbnreqI_pJ-g8F1qoXAkagI1iF)

      #### [Second Floor Plan](https://drive.google.com/open?id=1Lc4_4zchT0dUAXWIcSLuTxA8LRafECZk)
  - template: block__column
    component: column
    content: |-
      **What's the plan?**

      To increase the space on our campus by an additional 4,000 square feet. Additional space is needed for the treatment of children who have survived abuse and neglect. The additional space will include a welcoming, private waiting room for these clients, and space for support group meetings, and for family therapy. The center must also further increase accessibility for those with physical handicaps and cognitive impairments as we are committed to providing intersectional and inclusive services for all children. In order to achieve our expansion goals and provided the highest level of care for these families we need to raise $1,000,000 - we are half-way there! With your support, we can make an even greater impact on the lives of children in Genesee County.

      **Growing Demand**

      The tragic news is the demand for our services is growing significantly. Last year there were 1200 individuals seen and treated in our Genesee County location. In the first quarter of 2020, the three staff therapists conducted a total of 700 therapy sessions.

      We have exhausted our rooms to meet for interviews and therapy sessions, with children on our waiting list for help.
    alignment: Left
    width: One Half
  title: ''
  description: ''
- template: block__cta
  component: cta
  title: Donate
  subtitle: Help us grow our capacity to reach even more children.
  background: Light Blue
  background_image: "/media/social-story-family-room.jpg"
  background_image_opacity: 0.25
  button:
    url: https://www.voicesforcac.org/donate
    text: Give Today
- template: block__columns
  component: columns
  columns:
  - template: block__column
    component: column
    width: One Third
    alignment: Center
    content: "![](/media/eddie.jpg)"
  - template: block__column
    component: column
    width: One Third
    alignment: Left
    content: "**When a Child Needs Justice, Hope & Healing**\n\nMeet Eddie. At the
      sweet age of four, he’s looking forward to starting kindergarten in the coming
      school year. But unlike his future classmates, he has a dark secret. The reason
      Eddie is so ready for school to start is because it is a way to escape his grandfather.
      School would give him the space to be someone different, to make friends, to
      not have to think about his what his Grandfather does when they are alone. Unfortunately,
      Eddie's story is a familiar one. Far too many children suffer abuse; physical
      abuse, sexual abuse, emotional abuse, neglect… sometimes abused in multiple
      ways. These children deserve justice, hope, and healing from the abuse they
      have experienced. In Genesee County 1 in 4 children will be abused or be victims
      of human trafficking.\n\n![](/media/eddie.png)  "
  - template: block__column
    component: column
    width: One Third
    alignment: Center
    content: "![](/media/drawing.jpg)"
  title: ''
  description: ''
- template: block__feature
  component: feature
  background_image_opacity: 1
  orientation: normal
  image: "/media/96.png"
  content: "# We're Nearly There!"
  title: ''
  featured_content: ''
  background: ''
  background_image: ''
- template: block__content
  component: content
  content: "# Thank you to all of our donors!"
- template: block__cards
  component: cards
  cards:
  - image: "/media/cs mott foundation.jpg"
    title: CS Mott Foundation
    subtitle: ''
    content: ''
    link: ''
    title_top: ''
  - image: "/media/100-women-logo_12.jpg"
    title_top: ''
    subtitle: ''
    content: ''
    link: ''
    title: Northville Women Who Care
  - image: "/media/Elga.png"
    title_top: ''
    title: ELGA Credit Union
    subtitle: ''
    content: ''
    link: ''
  - image: "/media/Curbco.jpg"
    title_top: ''
    title: Curbco, Inc.
    subtitle: ''
    content: ''
    link: ''

---
We are at 96% of our $1 Million Goal! We still need $42,000 to reach our goal. We are trying to raise the last $42,000 and we NEED YOUR HELP

Our Big Blue House (as it has been named by the children) is a child-friendly renovated house that truly feels like a home. Currently, we operate in a 4,000 square foot space. Our Big Blue House has been a great place to serve children and their families in Genesee County. To serve even more families we are looking to add on to the home we've already made here.

This expansion of services was born from the needs identified by the children and families we serve, as well as the community. The center is excited to be growing and increasing our capacity to serve the unique needs of child victims and their families. In order to best serve clients and meet local demand, Voices for Children campus and service-center must expand.
