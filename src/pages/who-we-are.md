---
title: Who We Are
template: page_with_blocks
position: Above
title_background: Orange
title_background_image: "/media/mi-pham-FtZL0r4DZYk-unsplash.jpg"
title_background_image_opacity: 0.25
blocks:
- template: block__cards
  component: cards
  cards:
  - title_top: ''
    title: Overview & Mission
    subtitle: ''
    content: Learn about our organization and mission
    link: "/who-we-are/overview-and-mission"
    image: ''
  - title_top: ''
    title: Staff
    subtitle: ''
    content: Meet the Voices team
    link: "/who-we-are/staff"
    image: ''
  - title_top: ''
    title: Board
    subtitle: ''
    content: See our board of civic and community leaders
    link: "/who-we-are/board"
    image: ''
  - image: ''
    title_top: ''
    title: Advisory Board
    subtitle: ''
    content: See our advisory board working on our strategic plan
    link: "/who-we-are/advisory-board"
  - title_top: ''
    title: History
    subtitle: ''
    content: Read about how we got to here
    link: "/who-we-are/history"
    image: ''
alt_text: ''
subtitle: ''

---
