// These are based on Bulma's default breakpoints
export const sizes = {
  mobile: '767px',
  tablet: '768px',
  desktop: '1024px',
  widescreen: '1216px',
  fullhd: '1408px',
}

export const breakpoints = {
  mobile: `(max-width: ${sizes.mobile})`,
  tablet: `(min-width: ${sizes.tablet})`,
  desktop: `(min-width: ${sizes.desktop})`,
  widescreen: `(min-width: ${sizes.widescreen})`,
  fullhd: `(min-width: ${sizes.fullhd})`,
}
