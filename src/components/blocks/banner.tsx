import React from 'react'
import Img from 'gatsby-image'

const Banner = ({ block }) => {
  return (
    block.image && block.image.childImageSharp && block.image.childImageSharp.fluid && (
      <Img
        fluid={block.image.childImageSharp.fluid}
        alt={block.alt_text}
        title={block.alt_text}
      />
    )
  )
}

export default Banner
