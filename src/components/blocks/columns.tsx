import React from 'react'
import Column from './column'
import Markdown from '../markdown'

const Columns = ({ block }) => (
  <div className="section">
    <div className="container">
      {block.title && <h2 className="title is-2">{block.title}</h2>}
      {block.description && <Markdown content={block.description} />}
      <div className="columns">
        {block.columns.map((item, idx) => (
          <Column key={idx} block={item} />
        ))}
      </div>
    </div>
  </div>
)

export default Columns
