import React from 'react'
import Img from 'gatsby-image'

import { getBackgroundClass } from '../../helpers/lib/get-background-class'
import Markdown from '../markdown'
import { BackgroundImage, StyledSection } from './hero'

const Feature = ({ block }) => {
  const sectionClassName = block.background
    ? `block-feature hero is-medium is-${getBackgroundClass(block.background)}`
    : 'block-feature hero is-medium'
  return (
    <StyledSection className={sectionClassName} background={block.background}>
      {block.background_image && (
        <BackgroundImage
          background_image={block.background_image}
          background_image_opacity={block.background_image_opacity}
        />
      )}
      <div className="hero-body">
        <div className="container">
          <div
            className={
              block.orientation && block.orientation === 'reverse'
                ? 'columns is-vcentered is-reversed'
                : 'columns is-vcentered'
            }
          >
            <div className="column is-one-half">
              {block.image && block.image.childImageSharp && block.image.childImageSharp.fluid && (
                <Img
                  fluid={block.image.childImageSharp.fluid}
                  alt={block.title}
                  title={block.title}
                />
              )}
              {block.featured_content && (
                <Markdown content={block.featured_content} />
              )}
            </div>
            <div className="column is-one-half">
              <h1 className="title is-3">{block.title}</h1>
              <Markdown content={block.content} />
            </div>
          </div>
        </div>
      </div>
    </StyledSection>
  )
}

export default Feature
