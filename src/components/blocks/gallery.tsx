import React from 'react'
import GalleryComponent from '../gallery'

const Gallery = ({ block }) => {
  if (block && (!block.images || block.images.length === 0)) {
    return <></>
  }
  return (
    <div className="section">
      <div className="container">
        <GalleryComponent {...block} />
      </div>
    </div>
  )
}

export default Gallery
