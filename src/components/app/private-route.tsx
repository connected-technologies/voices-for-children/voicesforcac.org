import React from 'react'
import PropTypes from 'prop-types'
import { navigate } from 'gatsby'

import { isLoggedIn } from '../../services/auth-service'

const PrivateRoute = ({ component: Component, county, ...rest }) => {
  if (!isLoggedIn(county) && window.location.pathname !== `/private`) {
    navigate(`/make-a-referral/${county}-county/login`)
    return null
  }

  return <Component {...rest} />
}

PrivateRoute.propTypes = {
  component: PropTypes.any.isRequired,
}

export default PrivateRoute
