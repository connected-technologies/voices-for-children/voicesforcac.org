import React, { useState } from 'react'
import { navigate, useStaticQuery, graphql } from 'gatsby'

import { handleLogin, isLoggedIn } from '../../services/auth-service'
import PageTitle from '../page-title'
import SEO from '../seo'
import { RouteComponentProps } from '@reach/router'

const COUNTY = 'shiawassee'

const ThePageTitle = () => {
  const data = useStaticQuery(graphql`
    {
      fileName: file(
        relativePath: { eq: "sharon-mccutcheon-Vl0KHsz67kE-unsplash.jpg" }
      ) {
        childImageSharp {
          fluid(maxWidth: 1600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  return (
    <PageTitle
      title="Make a Referral: Shiawassee County"
      subtitle="Authorization is Required"
      title_background="Blue"
      title_background_image={data.fileName}
      title_background_image_opacity={0.5}
    />
  )
}

const Login = (props: RouteComponentProps) => {
  const [state, setState] = useState({
    password: '',
    error: '',
  })
  const handleUpdate = event => {
    setState({
      ...state,
      [event.target.name]: event.target.value,
    })
  }
  const handleSubmit = event => {
    event.preventDefault()
    const result = handleLogin({ county: COUNTY, password: state.password })
    if (result) {
      setState({
        ...state,
        error: '',
      })
      navigate(`/make-a-referral/${COUNTY}-county`)
    } else {
      setState({
        ...state,
        error: 'We are unable to authenticate you. Please try again.',
      })
    }
  }

  if (isLoggedIn(COUNTY)) {
    navigate(`/make-a-referral/${COUNTY}-county`)
  }
  return (
    <>
      <SEO title="Login | Make a Referral: Shiawassee County" />
      <ThePageTitle />
      <section className="section page-content">
        <main className="container content-container">
          <div className="content">
            <h2>Login</h2>
            <div className="columns">
              <div className="column is-one-third">
                <form
                  method="post"
                  onSubmit={event => {
                    handleSubmit(event)
                  }}
                >
                  <div className="field">
                    <label className="label" htmlFor="password">
                      Password
                    </label>
                    <div className="control">
                      <input
                        className={state.error ? 'input is-danger' : 'input'}
                        type="password"
                        name="password"
                        id="password"
                        onChange={handleUpdate}
                      />
                    </div>
                    {state.error && (
                      <p className="help is-danger">{state.error}</p>
                    )}
                  </div>

                  <div className="field is-grouped">
                    <div className="control">
                      <input
                        className="button is-primary"
                        type="submit"
                        value="Login"
                        disabled={!state.password}
                      />
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </main>
      </section>
    </>
  )
}

export default Login
