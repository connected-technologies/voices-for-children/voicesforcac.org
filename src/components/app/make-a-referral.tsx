import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import PageTitle from '../page-title'
import SEO from '../seo'
import { RouteComponentProps } from '@reach/router'
import Card from '../card'

const CARDS = [
  {
    title: 'Genesee County',
    content:
      'If you are an MDT partner, making a referral for a forensic interview for an investigation in Genesee County, please complete this referral form.',
    link: '/make-a-referral/genesee-county',
  },
  {
    title: 'Shiawassee County',
    content:
      'If you are an MDT partner, making a referral for a forensic interview for an investigation in Shiawassee County, please complete this referral form.',
    link: '/make-a-referral/shiawassee-county',
  },
]

const ThePageTitle = () => {
  const data = useStaticQuery(graphql`
    {
      fileName: file(
        relativePath: { eq: "sharon-mccutcheon-Vl0KHsz67kE-unsplash.jpg" }
      ) {
        childImageSharp {
          fluid(maxWidth: 1600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  return (
    <PageTitle
      title="Make a Referral"
      title_background="Purple"
      title_background_image={data.fileName}
      title_background_image_opacity={0.5}
    />
  )
}

const MakeAReferral = (props: RouteComponentProps) => {
  return (
    <>
      <SEO title="Make a referral" />
      <ThePageTitle />
      <section className="section page-content">
        <main className="container content-container">
          <div className="content">
            <div className="section">
              <div className="container">
                <div className="columns is-multiline is-centered">
                  {CARDS.map((cardData, idx) => {
                    return (
                      <div key={`card-${idx}`} className="column is-6">
                        <Card cardData={cardData} />
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </div>
        </main>
      </section>
    </>
  )
}

export default MakeAReferral
