import React from 'react'
import styled, { css } from 'styled-components'

import { breakpoints } from '../breakpoints'
import variables from '../scss/variables.scss'
import { getBackgroundClass } from '../helpers/lib/get-background-class'
import { getBackgroundColor } from '../helpers/lib/get-background-color'

export interface PageTitleProps {
  title: string
  subtitle?: string
  title_background?: string
  title_background_image?: string
  title_background_image_opacity?: number
}

const Wrapper = styled.div`
  position: relative;
  background-color: ${({ theme, title_background }) =>
    getBackgroundColor(title_background, theme)};
  ${({ title_background_image }) =>
    title_background_image
      ? css`
          padding: 4rem 1.5rem;
        `
      : css`
          margin-top: 1.5rem;
          padding: 0 1.5rem;
        `};
  @media ${breakpoints.mobile} {
    text-align: center;
  }
`

const BackgroundImage = styled.div`
  ${({ title_background_image }) =>
    title_background_image
      ? css`
          background-image: url(${title_background_image.childImageSharp.fluid
            .src});
        `
      : ''};

  background-repeat: no-repeat;
  background-position: 50% 50%;
  background-size: cover;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  opacity: ${({ title_background_image_opacity }) =>
    title_background_image_opacity || 1};
`

const Title = styled.h1`
  font-family: ${({ theme }) => theme.familyTertiary};
  color: ${({ theme, title_background }) =>
    getBackgroundClass(title_background) === 'dark'
      ? theme.whitesmoke
      : theme.eclipse};
  margin: 0;
  /* text-shadow: -1px -1px 0 ${variables.eclipse}, 1px -1px 0 ${
  variables.eclipse
},
    -1px 1px 0 ${variables.eclipse}, 1px 1px 0 ${variables.eclipse}; */
`

const Subtitle = styled.h2`
  color: ${({ theme, title_background }) =>
    getBackgroundClass(title_background) === 'dark'
      ? theme.whitesmoke
      : theme.eclipse};
  font-size: 2rem;
  font-weight: 400;
  line-height: 1.25;
  margin: 0;
  /* text-shadow: -1px -1px 0 ${variables.eclipse}, 1px -1px 0 ${
  variables.eclipse
},
    -1px 1px 0 ${variables.eclipse}, 1px 1px 0 ${variables.eclipse}; */
  word-break: break-word;
`

const PageTitle = ({
  title,
  subtitle,
  title_background,
  title_background_image,
  title_background_image_opacity,
}: PageTitleProps) => {
  return (
    <Wrapper
      title_background={title_background}
      title_background_image={title_background_image}
    >
      {title_background_image && (
        <BackgroundImage
          title_background_image={title_background_image}
          title_background_image_opacity={title_background_image_opacity}
        />
      )}
      <div className="container">
        <Title title_background={title_background}>{title}</Title>
        {subtitle && (
          <Subtitle title_background={title_background}>{subtitle}</Subtitle>
        )}
      </div>
    </Wrapper>
  )
}

export default PageTitle
