import styled from 'styled-components'

import { CardElement } from '@stripe/react-stripe-js'

const StyledCardElement = styled(CardElement)`
  color: #363636;
  background-color: white;
  border: 1px solid #dbdbdb;
  border-radius: 4px;
  box-shadow: inset 0 0.0625em 0.125em rgba(10, 10, 10, 0.05);
  padding-bottom: calc(0.75em - 1px);
  padding-left: calc(0.75em - 1px);
  padding-right: calc(0.75em - 1px);
  padding-top: calc(0.75em - 1px);
  max-height: 40px;
  &.StripeElement--invalid {
    border-color: #f14668;
  }
`

export default StyledCardElement
