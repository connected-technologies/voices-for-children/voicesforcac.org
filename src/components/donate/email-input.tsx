import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faEnvelope,
  faExclamationTriangle,
} from '@fortawesome/free-solid-svg-icons'

const EmailInput = ({
  label,
  name,
  placeholder,
  register,
  required,
  error,
}) => (
  <div className="field">
    <label className="label">{label}</label>
    <div className="control has-icons-left has-icons-right">
      <input
        type="email"
        className={`input ${error ? 'is-danger' : ''}`}
        name={name}
        placeholder={placeholder}
        ref={register({ required })}
      />
      <span className="icon is-small is-left">
        <FontAwesomeIcon icon={faEnvelope} />
      </span>
      {error && (
        <span className="icon is-small is-right has-text-danger">
          <FontAwesomeIcon icon={faExclamationTriangle} />
        </span>
      )}
    </div>
    {error && <div className="help is-danger">{error}</div>}
  </div>
)

export default EmailInput
