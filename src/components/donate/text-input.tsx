import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons'

export interface TextInputProps {
  label: string
  name: string
  placeholder: string
  register: any
  required?: boolean
  error?: string
  help?: string
}

const TextInput = ({
  label,
  name,
  placeholder,
  register,
  required,
  error,
  help,
}: TextInputProps) => (
  <div className="field">
    <label className="label">{label}</label>
    <div className="control has-icons-right">
      <input
        className={`input ${error ? 'is-danger' : ''}`}
        name={name}
        placeholder={placeholder}
        ref={register({ required })}
      />
      {error && (
        <span className="icon is-small is-right has-text-danger">
          <FontAwesomeIcon icon={faExclamationTriangle} />
        </span>
      )}
    </div>
    {help && <div className="help">{help}</div>}
    {error && <div className="help is-danger">{error}</div>}
  </div>
)

export default TextInput
