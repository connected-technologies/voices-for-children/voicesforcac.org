import React from 'react'
import styled from 'styled-components'
import { darken } from 'polished'
import Loadable from 'react-loadable'

import Column from './blocks/column'
import Social from './social'
import Loading from './loading'

const LoadableComponent = Loadable({
  loader: () => import('./exit'),
  loading: Loading,
})

const StyledFooter = styled.footer`
  background: ${props => props.theme.dark};
  .content {
    color: ${props => props.theme.light};
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      color: ${props => props.theme.light};
    }
    a {
      color: ${props => props.theme.vfcLightBlue};
      &:hover {
        color: ${props => darken(0.1, props.theme.vfcLightBlue)};
      }
    }
  }
`

const Footer = ({ content }) => (
  <StyledFooter className="section">
    <div className="container">
      <div className="columns has-text-centered-touch">
        {content.map((item, idx) => (
          <Column key={idx} block={item} />
        ))}
        <div className="column">
          <p className="has-text-white">
            Voices for Children does not discriminate in the delivery of
            services or benefits based on race, color, national origin,
            religion, sex, disability, and age as well as sexual orientation and
            gender identity.
          </p>
          <p className="mt-4">
            <a href="/media/Public Acceptance Policy.pdf" target="_blank">
              View Public Disclosure Agreement.
            </a>
          </p>
        </div>
        <div className="column">
          <Social />
        </div>
      </div>
    </div>
    <LoadableComponent />
  </StyledFooter>
)

export default Footer
