import React, { useState } from 'react'
import styled from 'styled-components'

import Logo from '../logo'
import NavbarMenu from './navbar-menu'
import MobileMenu from './mobile-menu'

const LogoContainer = styled.div`
  height: 56px;
  width: 108px;
`

const Navbar = () => {
  const [state, setState] = useState({
    navbarOpen: false,
  })

  const toggleNavState = () => {
    setState({
      ...state,
      navbarOpen: !state.navbarOpen,
    })
  }

  return (
    <>
      <nav
        className="navbar is-fixed-top is-spaced has-shadow"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <a className="navbar-item" href="/">
            <LogoContainer>
              <Logo />
            </LogoContainer>
          </a>

          <span
            onClick={toggleNavState}
            onKeyPress={toggleNavState}
            role="button"
            tabIndex={0}
            className={
              state.navbarOpen
                ? 'navbar-burger burger is-active'
                : 'navbar-burger burger'
            }
            aria-label="menu"
            aria-expanded="false"
            data-target="mainMenu"
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </span>
        </div>

        <NavbarMenu
          id="mainMenu"
          className="navbar-menu"
          // className={state.navbarOpen ? 'navbar-menu is-active' : 'navbar-menu'}
          // onClick={toggleNavState}
          // onKeyPress={toggleNavState}
        />
      </nav>
      <MobileMenu state={state} toggleNavState={toggleNavState} />
    </>
  )
}

export default Navbar
