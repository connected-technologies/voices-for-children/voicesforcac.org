import React from 'react'
import Helmet from 'react-helmet'
import { useStaticQuery, graphql } from 'gatsby'

export interface SeoMeta {
  name?: string
  content?: string
  property?: string
}

export interface SeoProps {
  description?: string
  lang?: string
  meta?: SeoMeta[]
  title?: string
  image?: string
}

const SEO = ({ description, lang, meta, title, image }: SeoProps) => {
  const { site } = useStaticQuery(
    graphql`
      {
        site {
          ...SiteMetadata
        }
      }
    `
  )

  const metaDescription = description || site.siteMetadata.description
  const metaImage = image
    ? `${site.siteMetadata.siteUrl}${image}`
    : `${site.siteMetadata.siteUrl}${site.siteMetadata.logo}`

  const defaultMeta: SeoMeta[] = [
    {
      name: `description`,
      content: metaDescription,
    },
    {
      property: `og:title`,
      content: title,
    },
    {
      property: `og:description`,
      content: metaDescription,
    },
    {
      property: `og:image`,
      content: metaImage,
    },
    {
      property: `og:type`,
      content: `website`,
    },
    {
      name: `twitter:card`,
      content: `summary`,
    },
    {
      name: `twitter:title`,
      content: title,
    },
    {
      name: `twitter:description`,
      content: metaDescription,
    },
    {
      property: `twitter:image`,
      content: metaImage,
    },
  ]
  return (
    <Helmet
      htmlAttributes={{
        lang,
      }}
      title={title}
      titleTemplate={`%s | ${site.siteMetadata.title}`}
      meta={defaultMeta.concat(meta as SeoMeta[])}
    />
  )
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  description: ``,
}

export default SEO
