import React, { useState, CSSProperties, useEffect } from 'react'
import ReactMapGL, { Marker, Popup, NavigationControl } from 'react-map-gl'

import MapMarker from './marker'
import MapPopup from './popup'

import 'mapbox-gl/dist/mapbox-gl.css'

const TOKEN = process.env.GATSBY_MAPBOX_TOKEN

export interface PopupInfo {
  latitude: string
  longitude: string
  popup_content: string
}

export interface Viewport {
  width: string
  height: string
  latitude: number
  longitude: number
  zoom: number
  bearing: number
  pitch: number
}

export interface MapState {
  viewport: Viewport
  popupInfo: PopupInfo | null
  mapStyle: string
  markers: {
    longitude: number
    latitude: number
  }[]
}

const navStyle: CSSProperties = {
  position: 'absolute',
  top: 0,
  left: 0,
  padding: '10px',
}

const Map = props => {
  const { latitude, longitude } = props
  const defaultState: MapState = {
    viewport: {
      width: '100%',
      height: '100%',
      latitude,
      longitude,
      zoom: 12,
      bearing: 0,
      pitch: 0,
    },
    popupInfo: null,
    mapStyle: 'mapbox://styles/mapbox/streets-v10',
    markers: props.markers,
  }
  const [state, setState] = useState(defaultState)

  const onViewportChange = viewport => {
    setState({
      ...state,
      viewport: { ...state.viewport, ...viewport },
    })
  }

  const resize = () => {
    console.log('resize')
    onViewportChange({
      width: '100%',
      height: '100%',
    })
  }

  const renderCityMarker = (marker, index) => {
    return (
      <Marker
        key={`marker-${index}`}
        longitude={Number(marker.longitude)}
        latitude={Number(marker.latitude)}
      >
        <MapMarker
          size={54}
          onClick={() => setState({ ...state, popupInfo: marker })}
        />
      </Marker>
    )
  }

  const renderPopup = () => {
    const popupInfo = state.popupInfo

    return (
      popupInfo && (
        <Popup
          tipSize={10}
          anchor="top"
          longitude={Number(popupInfo.longitude)}
          latitude={Number(popupInfo.latitude)}
          closeOnClick={false}
          onClose={() => setState({ ...state, popupInfo: null })}
        >
          <MapPopup content={popupInfo.popup_content} />
        </Popup>
      )
    )
  }

  const { mapStyle, viewport, markers } = state
  return (
    <ReactMapGL
      {...viewport}
      mapStyle={mapStyle}
      onViewportChange={viewport => onViewportChange(viewport)}
      mapboxApiAccessToken={TOKEN}
      touchAction="pan-y"
      scrollZoom={false}
      dragRotate={false}
    >
      {markers.map(renderCityMarker)}
      {renderPopup()}
      <div className="nav" style={navStyle}>
        <NavigationControl showCompass={false} />
      </div>
    </ReactMapGL>
  )
}

export default Map
