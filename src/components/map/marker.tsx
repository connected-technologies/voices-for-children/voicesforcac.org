import React from 'react'

import theme from '../../theme'

// Original icon & viewBox
// const ICON = `M20.2,15.7L20.2,15.7c1.1-1.6,1.8-3.6,1.8-5.7c0-5.6-4.5-10-10-10S2,4.5,2,10c0,2,0.6,3.9,1.6,5.4c0,0.1,0.1,0.2,0.2,0.3
// c0,0,0.1,0.1,0.1,0.2c0.2,0.3,0.4,0.6,0.7,0.9c2.6,3.1,7.4,7.6,7.4,7.6s4.8-4.5,7.4-7.5c0.2-0.3,0.5-0.6,0.7-0.9
// C20.1,15.8,20.2,15.8,20.2,15.7z`
// const viewBox = "0 0 24 24"

// FA icons
const ICON = `M1152 640q0-106-75-181t-181-75-181 75-75 181 75 181 181 75 181-75 75-181zm256 0q0 109-33 179l-364 774q-16 33-47.5 52t-67.5 19-67.5-19-46.5-52l-365-774q-33-70-33-179 0-212 150-362t362-150 362 150 150 362z`
// const ICON = `M896 1088q66 0 128-15v655q0 26-19 45t-45 19h-128q-26 0-45-19t-19-45v-655q62 15 128 15zm0-1088q212 0 362 150t150 362-150 362-362 150-362-150-150-362 150-362 362-150zm0 224q14 0 23-9t9-23-9-23-23-9q-146 0-249 103t-103 249q0 14 9 23t23 9 23-9 9-23q0-119 84.5-203.5t203.5-84.5z`
const viewBox = '0 0 1792 1792'

const pinStyle = {
  cursor: 'pointer',
  fill: theme.vfcRed,
  stroke: 'none',
}

const Marker = props => {
  const { size = 20, onClick } = props

  return (
    <svg
      height={size}
      viewBox={viewBox}
      style={{
        ...pinStyle,
        transform: `translate(${-size / 2}px,${-size}px)`,
      }}
      onClick={onClick}
    >
      <path d={ICON} />
    </svg>
  )
}

export default Marker
