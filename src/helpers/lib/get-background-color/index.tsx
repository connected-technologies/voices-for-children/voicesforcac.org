export const getBackgroundColor = (backgroundColor, theme) => {
  switch (backgroundColor) {
    case 'Blue':
      return theme.vfcBlue
    case 'Purple':
      return theme.vfcPurple
    case 'Red':
      return theme.vfcRed
    case 'Green':
      return theme.vfcGreen
    case 'Orange':
      return theme.vfcOrange
    case 'Light Blue':
      return theme.vfcLightBlue
    case 'Dark':
      return theme.eclipse
    case 'Light':
      return theme.whitesmoke
    default:
      return 'transparent'
  }
}
