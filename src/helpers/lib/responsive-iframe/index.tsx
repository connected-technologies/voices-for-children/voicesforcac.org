import visit from 'unist-util-visit'
import _ from 'lodash'

const isPixelNumber = n => /\d+px$/.test(n)

const isUnitlessNumber = n => {
  const nToNum = _.toNumber(n)
  return _.isFinite(nToNum)
}

const isUnitlessOrPixelNumber = n =>
  n && (isUnitlessNumber(n) || isPixelNumber(n))

const acceptedDimensions = (width, height) =>
  isUnitlessOrPixelNumber(width) && isUnitlessOrPixelNumber(height)

const getStyleObject = style => {
  const regex = /([\w-]*)\s*:\s*([^;]*)/g
  let match
  const properties = {}
  while ((match = regex.exec(style))) {
    properties[match[1]] = match[2].trim()
  }
  return properties
}

const getStyleString = style =>
  Object.entries(style).reduce((styleString, [propName, propValue]) => {
    return `${styleString}${propName}: ${propValue}; `
  }, '')

export default () => {
  return tree => {
    visit(tree, 'element', (node, index, parent) => {
      if (node.tagName === 'iframe') {
        const { width, height } = node.properties
        if (acceptedDimensions(width, height)) {
          node.properties.style = getStyleString({
            ...getStyleObject(node.properties.style),
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
          })
          parent.children[index] = {
            type: 'element',
            tagName: 'div',
            properties: {
              style: `padding-bottom: ${(height / width) *
                100}%; position: relative; height: 0; overflow: hidden;`,
            },
            children: [node],
          }
        }
      }
    })
  }
}
