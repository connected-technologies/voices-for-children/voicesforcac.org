import React from 'react'
import format from 'rehype-format'
import raw from 'rehype-raw'
import rehype2react from 'rehype-react'
import stringify from 'rehype-stringify'
import remark from 'remark'
import remark2rehype from 'remark-rehype'

import Link from '../components/link'
import responsiveIframe from './lib/responsive-iframe'

export const markdownToHtml = content =>
  remark()
    .use(remark2rehype, { allowDangerousHTML: true })
    .use(raw)
    .use(responsiveIframe)
    .use(format)
    .use(stringify)
    .use(rehype2react, {
      createElement: React.createElement,
      components: { a: Link },
    })
    .processSync(content).contents
