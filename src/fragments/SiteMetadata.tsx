import { graphql } from 'gatsby'

export const SiteMetadata = graphql`
  fragment SiteMetadata on Site {
    siteMetadata {
      description
      footer {
        component
        content
      }
      logo
      siteUrl
      social {
        type
        link
      }
      title
    }
  }
`
