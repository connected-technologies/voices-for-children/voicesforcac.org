import Cookies from 'js-cookie'

// import * as siteConfig from '../../site-config.json'

const SITE_CONFIG = {
  genesee: '8102383333',
  shiawassee: '9897235877',
}

const isBrowser = typeof window !== `undefined`

const getIsLoggedIn = (county: string) => Cookies.get(`isLoggedIn-${county}`)

const setIsLoggedIn = (county: string) =>
  Cookies.set(`isLoggedIn-${county}`, true)

const clearIsLoggedIn = (county: string) =>
  Cookies.remove(`isLoggedIn-${county}`)

export const handleLogin = ({ county, password }) => {
  if (!isBrowser) return false

  if (String(password) === SITE_CONFIG[county]) {
    setIsLoggedIn(county)
    return true
  }

  return false
}

export const isLoggedIn = (county: string) => {
  if (!isBrowser) return false

  return getIsLoggedIn(county) === 'true'
}

export const logout = ({ county, callback }) => {
  if (!isBrowser) return

  clearIsLoggedIn(county)
  callback()
}
